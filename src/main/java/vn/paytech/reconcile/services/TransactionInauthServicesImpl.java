package vn.paytech.reconcile.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.ewallet.entity.TransactionInauth;
import vn.paytech.reconcile.ewallet.repository.TransactionInauthRepository;

import java.util.List;

@Service
public class TransactionInauthServicesImpl implements TransactionInauthServices {

    @Autowired
    private TransactionInauthRepository transactionInauthRepository;

    @Override
    public List<TransactionInauth> lstTransCurrentDate() {
        return transactionInauthRepository.lstTransCurrentDate();
    }

    @Override
    public TransactionInauth transByTransId(String transId) {
        return transactionInauthRepository.transByTransId(transId);
    }

    @Override
    public List<TransactionInauth> lstPaymentTransCurrentDateByMerchant(String merchantCode) {
        return transactionInauthRepository.lstPaymentTransCurrentDateByMerchant(merchantCode);
    }

    @Override
    public TransactionInauth transByRefNo(String refNo) {
        // TODO Auto-generated method stub
        return transactionInauthRepository.transByRefNo(refNo);
    }

    @Override
    public List<Object[]> lstTransLastDay() {
        return transactionInauthRepository.lstTransLastDay();
    }

    @Override
    public List<Object[]> lstTransLastDay(String fromDate, String toDate) {
        return transactionInauthRepository.lstTransLastDay(fromDate, toDate);
    }

    @Override
    public List<TransactionInauth> lstPaymentTransByMerchant(String merchantCode, String fromDate, String toDate) {
        return transactionInauthRepository.lstPaymentTransByMerchant(
                merchantCode
                , fromDate
                , toDate);
    }
}
