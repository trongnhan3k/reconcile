package vn.paytech.reconcile.services;

import java.util.List;

import vn.paytech.reconcile.ewallet.entity.TransactionInauth;

public interface TransactionInauthServices {

    List<TransactionInauth> lstTransCurrentDate();
    
    TransactionInauth transByTransId(String transId);
    
    List<TransactionInauth> lstPaymentTransCurrentDateByMerchant(String merchantCode);
    
    List<TransactionInauth> lstPaymentTransByMerchant(String merchantCode, String fromDate, String toDate);
    
    TransactionInauth transByRefNo(String refNo);


    List<Object[]> lstTransLastDay();

    List<Object[]> lstTransLastDay(String fromDate, String toDate);
    //TransactionInauth transCashInByNapasByC();
}
