package vn.paytech.reconcile.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.ewallet.repository.AccountsRepository;

@Service
public class AccountsServicesImpl implements AccountsServices {

    @Autowired
    private AccountsRepository accountsRepository;

    @Override
    public Double totalBalanceAmount() {
        return accountsRepository.totalBalanceAmount();
    }

}
