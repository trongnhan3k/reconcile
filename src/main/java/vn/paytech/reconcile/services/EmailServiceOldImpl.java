package vn.paytech.reconcile.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.config.LoadFileProperties;
import vn.paytech.reconcile.utils.BeanUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;


@Service
public class EmailServiceOldImpl implements EmailServiceOld {

    private static final Logger log = Logger.getLogger(EmailServiceOldImpl.class);

    private static final String NOREPLY_ADDRESS = LoadFileProperties.EmailUsername == null ? "no-reply@vitapay.vn" : LoadFileProperties.EmailUsername;

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SimpleMailMessage template;

//    @Autowired
//    private SpringTemplateEngine thymeleafTemplateEngine;
//
//    @Autowired
//    private FreeMarkerConfigurer freemarkerConfigurer;

    @Value("classpath:/mail-logo.png")
    private Resource resourceFile;

//    @Override
//    public void sendSimpleMessage(String to, String subject, String text) {
//    	if (emailSender == null)
//    		emailSender = BeanUtils.getBean(JavaMailSender.class);
//        try {
//            SimpleMailMessage message = new SimpleMailMessage();
//            message.setFrom(NOREPLY_ADDRESS);
//            message.setTo(to);
//            message.setSubject(subject);
//            message.setText(text);
//
//            emailSender.send(message);
//        } catch (MailException exception) {
//            exception.printStackTrace();
//        }
//    }

//    @Override
//    public void sendSimpleMessageUsingTemplate(String to,
//                                               String subject,
//                                               String ...templateModel) {
//        String text = String.format(template.getText(), templateModel);  
//        sendSimpleMessage(to, subject, text);
//    }

    @Override
    public void sendMessageWithAttachment(String[] to,
                                          String[] cc,
                                          String subject,
                                          String text,
                                          List<String> pathToAttachment) throws JsonProcessingException {
        if (emailSender == null)
            emailSender = BeanUtils.getBean(JavaMailSender.class);
        try {

            MimeMessage message = emailSender.createMimeMessage();
            // pass 'true' to the constructor to create a multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(NOREPLY_ADDRESS);

            helper.setTo(to);
            //helper.setCc(cc);
            helper.setBcc(cc);

            helper.setSubject(subject);
            helper.setText(text);

            for (String filePath : pathToAttachment) {
            	String[] splits = filePath.split("/");
                FileSystemResource file = new FileSystemResource(new File(filePath));
                helper.addAttachment(String.format("%s-%s", splits[splits.length - 2] ,file.getFilename()), file);
            }

            emailSender.send(message);
        } catch (MessagingException e) {
            log.error(e.getMessage());
            log.error(new ObjectMapper().writeValueAsString(e));
            e.printStackTrace();
        }
    }


    //    @Override
//    public void sendMessageUsingThymeleafTemplate(
//        String to, String subject, Map<String, Object> templateModel)
//            throws MessagingException {
//                
//        Context thymeleafContext = new Context();
//        thymeleafContext.setVariables(templateModel);
//        
//        String htmlBody = thymeleafTemplateEngine.process("template-thymeleaf.html", thymeleafContext);
//
//        sendHtmlMessage(to, subject, htmlBody);
//    }
//
//    @Override
//    public void sendMessageUsingFreemarkerTemplate(
//        String to, String subject, Map<String, Object> templateModel)
//            throws IOException, TemplateException, MessagingException {
//
//        Template freemarkerTemplate = freemarkerConfigurer.createConfiguration().getTemplate("template-freemarker.ftl");
//        String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, templateModel);
//
//        sendHtmlMessage(to, subject, htmlBody);
//    }
//    
    private void sendHtmlMessage(String to, String subject, String htmlBody) throws MessagingException {
        if (emailSender == null)
            emailSender = BeanUtils.getBean(JavaMailSender.class);
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setFrom(NOREPLY_ADDRESS);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(htmlBody, true);
        helper.addInline("attachment.png", resourceFile);
        emailSender.send(message);
    }

}
