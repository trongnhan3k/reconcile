package vn.paytech.reconcile.services;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

/**
 * Created by Olga on 8/22/2016.
 */
public interface EmailServiceOld {
    //    void sendSimpleMessage(String to,
//                           String subject,
//                           String text);
//    void sendSimpleMessageUsingTemplate(String to,
//                                        String subject,
//                                        String ...templateModel);
    void sendMessageWithAttachment(String[] to,
                                   String[] cc,
                                   String subject,
                                   String text,
                                   List<String> pathToAttachment) throws JsonProcessingException;

//    void sendMessageUsingThymeleafTemplate(String to,
//                                           String subject,
//                                           Map<String, Object> templateModel) 
//            throws IOException, MessagingException;
//
//    void sendMessageUsingFreemarkerTemplate(String to,
//                                            String subject,
//                                            Map<String, Object> templateModel)
//            throws IOException, TemplateException, MessagingException;
}
