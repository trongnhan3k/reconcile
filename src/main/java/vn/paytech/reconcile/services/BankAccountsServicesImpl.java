package vn.paytech.reconcile.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.ewallet.repository.BankAccountsRepository;
import vn.paytech.reconcile.model.LinkBankModel;

import java.util.ArrayList;
import java.util.List;

@Service
public class BankAccountsServicesImpl implements BankAccountsServices {

    @Autowired
    private BankAccountsRepository bankAccountsRepository;

    @Override
    public List<LinkBankModel> totalLinkedBankUsers() {
        List<Object[]> objects = bankAccountsRepository.totalLinkedBankUsers();
        List<LinkBankModel> results = new ArrayList<>();
        for (Object[] object : objects) {
            LinkBankModel linkBankModel = new LinkBankModel();
            linkBankModel.setBankCode(object[0].toString());
            linkBankModel.setUsers(Integer.parseInt(object[1].toString()));
            results.add(linkBankModel);
        }
        return results;
    }

}
