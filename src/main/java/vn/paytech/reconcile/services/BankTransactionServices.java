package vn.paytech.reconcile.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import vn.paytech.reconcile.paygate.entity.BankTransaction;

public interface BankTransactionServices {

    List<BankTransaction> lstTrans(String date);
    
    BankTransaction transByTransId(String transId);
    
    List<BankTransaction> lstTransByMerchant(String merchant, String fromDate, String toDate);
    
    List<BankTransaction> findAllByMerchantNameAndDate(String merchant);
    
    BankTransaction transByIpnRespTxn(String ipnRespTxn);

    List<Object[]> lstTransLastDay();

    List<Object[]> lstTransLastDay(String fromDate, String toDate);
    
    List<BankTransaction> lstTransCurrentDate();
}
