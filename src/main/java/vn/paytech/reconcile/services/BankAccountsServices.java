package vn.paytech.reconcile.services;

import vn.paytech.reconcile.model.LinkBankModel;

import java.util.List;

public interface BankAccountsServices {

    List<LinkBankModel> totalLinkedBankUsers();
}
