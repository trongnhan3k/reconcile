package vn.paytech.reconcile.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.paygate.entity.EvnReconcileEmail;
import vn.paytech.reconcile.paygate.repository.EvnReconcileEmailRepository;

import java.util.List;

@Service
public class EvnReconcileEmailServicesImpl implements EvnReconcileEmailServices {

    @Autowired
    EvnReconcileEmailRepository evnReconcileEmailRepository;


    @Override
    public List<EvnReconcileEmail> listEmailByQhName(String qhName) {
        return evnReconcileEmailRepository.findByQhName(qhName);
    }
}
