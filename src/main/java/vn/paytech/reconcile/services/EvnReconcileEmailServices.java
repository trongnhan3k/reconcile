package vn.paytech.reconcile.services;

import vn.paytech.reconcile.paygate.entity.EvnReconcileEmail;

import java.util.List;

public interface EvnReconcileEmailServices {

    List<EvnReconcileEmail> listEmailByQhName(String qhName);
}
