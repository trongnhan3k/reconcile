package vn.paytech.reconcile.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.paygate.entity.BankTransaction;
import vn.paytech.reconcile.paygate.repository.BankTransactionRepository;

import java.util.List;

@Service
public class BankTransactionServicesImpl implements BankTransactionServices {

    @Autowired
    private BankTransactionRepository bankTransactionRepository;

    @Override
    public List<BankTransaction> lstTrans(String date) {
        return null;
    }

    @Override
    public BankTransaction transByTransId(String transId) {
        return bankTransactionRepository.findByTransId(transId);
    }

    @Override
    public List<BankTransaction> lstTransByMerchant(String merchant, String fromDate, String toDate) {
        List<BankTransaction> result = bankTransactionRepository.findAllByMerchantName(
                merchant
                , fromDate
                , toDate);
        return result;
    }

    @Override
    public List<Object[]> lstTransLastDay() {
        return bankTransactionRepository.lstTransLastDay();
    }

    @Override
    public List<Object[]> lstTransLastDay(String fromDate, String toDate) {
        return bankTransactionRepository.lstTransLastDay(fromDate, toDate);
    }

    @Override
    public BankTransaction transByIpnRespTxn(String ipnRespTxn) {
        // TODO Auto-generated method stub
        return bankTransactionRepository.findByIpnRespTxn(ipnRespTxn);
    }

    @Override
    public List<BankTransaction> lstTransCurrentDate() {
        // TODO Auto-generated method stub
        return bankTransactionRepository.findAllByCurrentDate();
    }

    @Override
    public List<BankTransaction> findAllByMerchantNameAndDate(String merchant) {
        // TODO Auto-generated method stub
        return bankTransactionRepository.findAllByMerchantNameAndDate(merchant);
    }
}
