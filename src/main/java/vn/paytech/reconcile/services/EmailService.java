package vn.paytech.reconcile.services;

import okhttp3.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.config.Constant;

import java.io.File;

@Service
public class EmailService {

    private static final Logger log = Logger.getLogger(EmailService.class);

    @Value("${emailapiUrl}")
    String emailApiUrl;

    public void sendEmail(String[] filePaths, String content, String subject, String[] receivers, String[] ccs
            , String[] bccs) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        MultipartBody.Builder body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("content", content)
                .addFormDataPart("sender", Constant.RECONCILE_SENDER)
                .addFormDataPart("subject", subject)
                .addFormDataPart("isHtml", "false");

        if (receivers == null || receivers.length == 0) {
            return;
        } else {
            for (String receiver : receivers) {
                body.addFormDataPart("receivers ", receiver);
            }
        }

        if (filePaths != null && filePaths.length > 0) {
            for (String filePath : filePaths) {
                File file = new File(filePath);
                String[] splits = filePath.split("/");
                body.addFormDataPart("attachFiles"
                        , String.format("%s-%s", splits[splits.length - 2], file.getName(), file)
                        , RequestBody.create(MediaType
                                        .parse("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
                                file));
            }
        }

        if (ccs != null && ccs.length > 0) {
            for (String cc : ccs) {
                body.addFormDataPart("cc", cc);
            }
        }

        if (bccs != null && bccs.length > 0) {
            for (String bcc : bccs) {
                body.addFormDataPart("bcc", bcc);
            }
        }

        try {
            Request request = new Request.Builder()
                    .url(emailApiUrl)
                    .method("POST", body.build())
                    .build();
            Response response = client.newCall(request).execute();
            log.info("response: " + response.body().string());
        } catch (Exception e) {
        	e.printStackTrace();
            log.error("failed");
        }
    }

}
