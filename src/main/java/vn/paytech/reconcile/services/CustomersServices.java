package vn.paytech.reconcile.services;

import vn.paytech.reconcile.ewallet.entity.TransactionInauth;

import java.util.List;

public interface CustomersServices {

    List<Object[]> lstNewUsersLastDay();

    List<Object[]> lstNewUsersByDay(String fromDate, String toDate);
}
