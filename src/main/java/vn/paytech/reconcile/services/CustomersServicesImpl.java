package vn.paytech.reconcile.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.ewallet.entity.Customers;
import vn.paytech.reconcile.ewallet.repository.CustomersRepository;

import java.util.List;

@Service
public class CustomersServicesImpl implements CustomersServices {

    @Autowired
    private CustomersRepository customersRepository;

    @Override
    public List<Object[]> lstNewUsersLastDay() {
        return customersRepository.lstNewUsersLastDay();
    }

    @Override
    public List<Object[]> lstNewUsersByDay(String fromDate, String toDate) {
        return customersRepository.lstNewUsersByDay(fromDate, toDate);
    }
}
