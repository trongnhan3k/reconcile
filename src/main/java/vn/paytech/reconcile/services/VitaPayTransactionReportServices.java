package vn.paytech.reconcile.services;

import org.springframework.data.jpa.repository.Query;
import vn.paytech.reconcile.paygate.entity.VitaPayTransactionReport;

import java.util.List;

public interface VitaPayTransactionReportServices {

    void saveAll(List<VitaPayTransactionReport> vitaPayTransactionReport);

    void deleteData(String fromDate, String toDate);

    void deleteData();
}
