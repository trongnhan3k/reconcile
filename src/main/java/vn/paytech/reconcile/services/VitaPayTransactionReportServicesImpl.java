package vn.paytech.reconcile.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.paytech.reconcile.paygate.entity.VitaPayTransactionReport;
import vn.paytech.reconcile.paygate.repository.VitaPayTransactionReportRepository;

import java.util.List;

@Service
public class VitaPayTransactionReportServicesImpl implements VitaPayTransactionReportServices {

    @Autowired
    VitaPayTransactionReportRepository vitaPayTransactionReportRepository;

    @Override
    public void saveAll(List<VitaPayTransactionReport> vitaPayTransactionReport) {
        vitaPayTransactionReportRepository.saveAll(vitaPayTransactionReport);
    }

    @Override
    public void deleteData(String fromDate, String toDate) {
        vitaPayTransactionReportRepository.deleteData(fromDate, toDate);
    }

    @Override
    public void deleteData() {
        vitaPayTransactionReportRepository.deleteData();
    }
}
