package vn.paytech.reconcile;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import vn.paytech.reconcile.config.Constant;
import vn.paytech.reconcile.config.LoadFileProperties;
import vn.paytech.reconcile.controller.VitaPayReportController;
import vn.paytech.reconcile.quartz.EVNHaNoiByMonthReconcileJob;
import vn.paytech.reconcile.quartz.EVNHaNoiReconcileJob;
import vn.paytech.reconcile.quartz.EcomNapasReconcileJob;
import vn.paytech.reconcile.quartz.VitapayTransactionReconcileJob;
import vn.paytech.reconcile.quartz.WhiteLabelNapasReconcileJob;
import vn.paytech.reconcile.utils.DateUtils;

import javax.servlet.ServletOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.quartz.DateBuilder.evenMinuteDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import static spark.Spark.get;


@SpringBootApplication
public class ReconcileApplication implements CommandLineRunner {

    private static final Logger log = Logger.getLogger(ReconcileApplication.class);

    static {
        new LoadFileProperties();
    }


    public static void main(String[] args) throws ParseException {

        SpringApplication.run(ReconcileApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {

        get("/report/updateData", (req, res) -> {
            VitaPayReportController vitaPayReportController = new VitaPayReportController();
            String result = vitaPayReportController.updateData(req.raw());
            res.body(result);
            return res.body();
        });

        get("/report/getTransactionsData", (req, res) -> {
            VitaPayReportController vitaPayReportController = new VitaPayReportController();
            String result = vitaPayReportController.getTransactionsData(req.raw());
            res.body(result);
            return res.body();
        });

        get("/report/getCustomersData", (req, res) -> {
            VitaPayReportController vitaPayReportController = new VitaPayReportController();
            String result = vitaPayReportController.getUsersData(req.raw());
            res.body(result);
            return res.body();
        });

        get("/report/getLinkedBankUsersData", (req, res) -> {
            VitaPayReportController vitaPayReportController = new VitaPayReportController();
            String result = vitaPayReportController.getLinkedBankUsersData();
            res.body(result);
            return res.body();
        });

        get("/report/getTotalAccountBalanceAmount", (req, res) -> {
            VitaPayReportController vitaPayReportController = new VitaPayReportController();
            String result = vitaPayReportController.getTotalBalanceAmount();
            res.body(result);
            return res.body();
        });

        get("/report/getFileReconcileEvnHnMonthly", (req, res) -> {
            VitaPayReportController vitaPayReportController = new VitaPayReportController();
            InputStreamResource resource = vitaPayReportController.getFileReconcileEvnHnMonthly(req.raw(), res.raw());
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + req.raw().getParameter("qh") + ".xlsx\"")
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);


//                    ResponseEntity.ok()
//                    .contentType(MediaType.parseMediaType("application/octet-stream"))
//
//                    .body(resource);
        });


        get("/report/reconcile", (req, res) -> {
            String fromDate = req.raw().getParameter("fromDate");
            String toDate = req.raw().getParameter("toDate");
            Iterator iterator = Constant.mapQhEvn.entrySet().iterator();
            EVNHaNoiByMonthReconcileJob evnHaNoiByMonthReconcileJob = new EVNHaNoiByMonthReconcileJob();
            try {
                Map<String, List<String[]>> transactions = evnHaNoiByMonthReconcileJob.getData(fromDate, toDate);
                while (iterator.hasNext()) {
                    Map.Entry pair = (Map.Entry) iterator.next();
                    evnHaNoiByMonthReconcileJob.createExcelFile((String) pair.getValue()
                            , transactions.get(pair.getValue())
                            , fromDate
                            , toDate);
                    // iterator.remove(); // avoids a ConcurrentModificationException
                }
                return ResponseEntity.ok("success");
            } catch (Exception e) {
                return ResponseEntity.badRequest();
            }
        });

        log.info("------- Initializing ----------------------");


        SchedulerFactory sf = new StdSchedulerFactory();
        Scheduler sched = sf.getScheduler();

        // computer a time that is on the next round minute
        Date runTime = evenMinuteDate(new Date());

//	    // define the job EcomNapasJob class
        JobDetail ecomNapasJob = newJob(EcomNapasReconcileJob.class).withIdentity("ecom_napas_reconcile_job", "reconcile").build();
//	    // define the job WLNapasJob class
        JobDetail wLNapasJob = newJob(WhiteLabelNapasReconcileJob.class).withIdentity("whitelabel_napas_reconcile_job", "reconcile").build();
//	    
        // define the job ReportJob class
        JobDetail reporDailytJob = newJob(VitapayTransactionReconcileJob.class).withIdentity("vitapay_report_daily_job", "reconcile").build();

        // define the job EVN class
        JobDetail EVNDailyJob = newJob(EVNHaNoiReconcileJob.class).withIdentity("vitapay_evn_report_daily_job", "reconcile").build();

        // define the job EVN class
        JobDetail EVNMonthJob = newJob(EVNHaNoiByMonthReconcileJob.class).withIdentity("vitapay_evn_report_month_job", "reconcile").build();

//	    // Trigger the job to run on the next round minute
        Trigger ecomNapasTrigger = newTrigger().withIdentity("ecom_napas_reconcile_job", "reconcile")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 20 10 * * ?"))
                //.startAt(runTime)
                .build();
        Trigger wLNapasTrigger = newTrigger().withIdentity("whitelabel_napas_reconcile_job", "reconcile")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 20 10 * * ?"))
                //.startAt(runTime)
                .build();

        Trigger reportDailyTrigger = newTrigger().withIdentity("vitapay_report_daily_job", "reconcile")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 59 23 * * ?"))
                //.startAt(runTime)
                .build();

        Trigger EVNDailyTrigger = newTrigger().withIdentity("vitapay_evn_report_daily_job", "reconcile")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 4 * * ?"))
//                .startAt(runTime)
                .build();

        //evnHn monthly for every first day of the month from JAN to NOV
        Trigger EVNMonthTrigger = newTrigger().withIdentity("vitapay_evn_report_month_job", "reconcile")
//                .startAt(runTime)
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 5 1 1-11 ?")) //ngay 1 hang thang tu thang 1 den 11
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 5 21 12 ?")) //ngay 21 thang 12 hang nam
                .build();

        sched.start();


        sched.scheduleJob(ecomNapasJob, ecomNapasTrigger);
        sched.scheduleJob(wLNapasJob, wLNapasTrigger);
        sched.scheduleJob(reporDailytJob, reportDailyTrigger);
        sched.scheduleJob(EVNDailyJob, EVNDailyTrigger);
        sched.scheduleJob(EVNMonthJob, EVNMonthTrigger);

    }

}
