package vn.paytech.reconcile.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import vn.paytech.reconcile.config.LoadFileProperties;
import vn.paytech.reconcile.napas.model.NapasAcqModel;

public class FileUtils {

	public static void writeFileCompare(String dateFormatFile ,List<NapasAcqModel> compareModels, String service, String fileStatus) {
		try {
			Date date = Calendar.getInstance().getTime();
			DateFormat dateContentFile = new SimpleDateFormat("ddMMyyyy");
			String strDate = dateFormatFile;// dateContentFile.format(date);
			// Tạo 1 file để hung toàn bộ kqua xứ lý theo mô tả trong file doc.
			String filePath = LoadFileProperties.Napas_localDir + dateFormatFile + "/" + LoadFileProperties.pathFile(dateFormatFile, "ACQ", service , fileStatus);
			filePath = filePath.substring(0, filePath.indexOf(".pgp"));
			if (!isCreateFile(filePath))
				return;

			FileWriter fstream = new FileWriter(filePath);
			BufferedWriter out = new BufferedWriter(fstream);
			Long amountLong = (long) 0;
			// header HR[REV] 971020[DATE]06032020
			String header = "HR[REV]" + StringUtils.leftPad(LoadFileProperties.Napas_SecretKey, 8)  +"[DATE]" + strDate;
			out.write(header);
			out.write("\n");
			// detail DR[MTI]0210[F2] 970400______0018[F3]000000[SVC]
			// EC_CASHIN[TCC]04[F4]000015000000[RTA]000015000000[F49]704[F5]000015000000[F50]704[F9]00000000[F6]000000000000[RCA]000000000000[F51]704[F10]00000000[F11]000000[F12]140516[F13]0305[F15]0306[F18]7399[F22]000[F25]08[F41]00005538[ACQ]
			// 971020[ISS] 970500[MID] PAYTECH[BNB] [F102] [F103]
			// [SVFISSNP]000000000000[IRFISSACQ]000000000000[IRFISSBNB]000000000000[SVFACQNP]000000000000[IRFACQISS]000000000000[IRFACQBNB]000000000000[SVFBNBNP]000000000000[IRFBNBISS]000000000000[IRFBNBACQ]000000000000[F37]
			// 835633111[F38] [TRN]AAAD2QDOWNo0pQAA[RRC]0000[RSV1] [RSV2] [RSV3]
			// [CSR]f3966d4e81fb878b947991db9d084eee
			int number = 0;
			
			for (NapasAcqModel item : compareModels) {
				item.setRRC("0117");
				String detail = String.format(LoadFileProperties.Napas_ContentFile, item.getMTI(), item.getF2(),
						item.getF3(), item.getSVC(), item.getTCC(), item.getF4(), item.getRTA(), item.getF49(),
						item.getF5(), item.getF50(), item.getF9(), item.getF6(), item.getRCA(), item.getF51(),
						item.getF10(), item.getF11(), item.getF12(), item.getF13(), item.getF15(), item.getF18(),
						item.getF22(), item.getF25(), item.getF41(), item.getACQ(), item.getISS(), item.getMID(),
						item.getBNB(), item.getF102(), item.getF103(), item.getF37(), item.getF38(), item.getTRN(),
						item.getRRC(), item.getRSV1());
				System.out.println(detail);
				String csr = EncryptDecryptManager.hashMD5Napas(detail);
				detail += csr;
				out.write(detail);
				out.write("\n");
				number++;
			}
			// trailer recored
			Date calendar = Calendar.getInstance().getTime();
			DateFormat dateFormat = new SimpleDateFormat("hhmmss");
			String strTime = dateFormat.format(calendar);
			String trailer = String.format("TR[NOT]%s[CRE]             PAYTECH[TIME]%s[DATE]%s[CSF]", StringUtils.leftPad(Integer.toString(number), 9, "0"), strTime,
					strDate);
			String csf = EncryptDecryptManager.hashMD5Napas(trailer);
			trailer += csf;
			out.write(trailer);

			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static List<NapasAcqModel> readFileNapas(String file) {
		List<String> strings = readFile(file);
		List<NapasAcqModel> models = new ArrayList<>();
		// read file into stream, try-with-resources
		strings.forEach(s -> {
			if (s.startsWith("HR") || s.startsWith("TR")) return;
			NapasAcqModel napasAcqModel = new NapasAcqModel();
			napasAcqModel.setMTI(s.substring(s.indexOf("[MTI]") + 5, s.lastIndexOf("[F2]")));
			napasAcqModel.setF2(s.substring(s.indexOf("[F2]") + 4, s.lastIndexOf("[F3]")));
			napasAcqModel.setF3(s.substring(s.indexOf("[F3]") + 4, s.lastIndexOf("[SVC]")));
			napasAcqModel.setSVC(s.substring(s.indexOf("[SVC]") + 5, s.lastIndexOf("[TCC]")));
			napasAcqModel.setTCC(s.substring(s.indexOf("[TCC]") + 5, s.lastIndexOf("[F4]")));
			napasAcqModel.setF4(s.substring(s.indexOf("[F4]") + 4, s.lastIndexOf("[RTA]")));
			napasAcqModel.setRTA(s.substring(s.indexOf("[RTA]") + 5, s.lastIndexOf("[F49]")));
			napasAcqModel.setF49(s.substring(s.indexOf("[F49]") + 5, s.lastIndexOf("[F5]")));
			napasAcqModel.setF5(s.substring(s.indexOf("[F5]") + 4, s.lastIndexOf("[F50]")));
			napasAcqModel.setF50(s.substring(s.indexOf("[F50]") + 5, s.lastIndexOf("[F9]")));
			napasAcqModel.setF9(s.substring(s.indexOf("[F9]") + 4, s.lastIndexOf("[F6]")));
			napasAcqModel.setF6(s.substring(s.indexOf("[F6]") + 4, s.lastIndexOf("[RCA]")));
			napasAcqModel.setRCA(s.substring(s.indexOf("[RCA]") + 5, s.lastIndexOf("[F51]")));
			napasAcqModel.setF51(s.substring(s.indexOf("[F51]") + 5, s.lastIndexOf("[F10]")));
			napasAcqModel.setF10(s.substring(s.indexOf("[F10]") + 5, s.lastIndexOf("[F11]")));
			napasAcqModel.setF11(s.substring(s.indexOf("[F11]") + 5, s.lastIndexOf("[F12]")));
			napasAcqModel.setF12(s.substring(s.indexOf("[F12]") + 5, s.lastIndexOf("[F13]")));
			napasAcqModel.setF13(s.substring(s.indexOf("[F13]") + 5, s.lastIndexOf("[F15]")));
			napasAcqModel.setF15(s.substring(s.indexOf("[F15]") + 5, s.lastIndexOf("[F18]")));
			napasAcqModel.setF18(s.substring(s.indexOf("[F18]") + 5, s.lastIndexOf("[F22]")));
			napasAcqModel.setF22(s.substring(s.indexOf("[F22]") + 5, s.lastIndexOf("[F25]")));
			napasAcqModel.setF25(s.substring(s.indexOf("[F25]") + 5, s.lastIndexOf("[F41]")));
			napasAcqModel.setF41(s.substring(s.indexOf("[F41]") + 5, s.lastIndexOf("[ACQ]")));
			napasAcqModel.setACQ(s.substring(s.indexOf("[ACQ]") + 5, s.lastIndexOf("[ISS]")));
			napasAcqModel.setISS(s.substring(s.indexOf("[ISS]") + 5, s.lastIndexOf("[MID]")));
			napasAcqModel.setMID(s.substring(s.indexOf("[MID]") + 5, s.lastIndexOf("[BNB]")));
			napasAcqModel.setBNB(s.substring(s.indexOf("[BNB]") + 5, s.lastIndexOf("[F102]")));
			napasAcqModel.setF102(s.substring(s.indexOf("[F102]") + 6, s.lastIndexOf("[F103]")));
			napasAcqModel.setF103(s.substring(s.indexOf("[F103]") + 6, s.lastIndexOf("[SVFISSNP]")));
			napasAcqModel.setSVFISSNP(s.substring(s.indexOf("[SVFISSNP]") + 10, s.lastIndexOf("[IRFISSACQ]")));
			napasAcqModel.setIRFISSACQ(s.substring(s.indexOf("[IRFISSACQ]") + 11, s.lastIndexOf("[IRFISSBNB]")));
			napasAcqModel.setIRFISSBNB(s.substring(s.indexOf("[IRFISSBNB]") + 11, s.lastIndexOf("[SVFACQNP]")));
			napasAcqModel.setSVFACQNP(s.substring(s.indexOf("[SVFACQNP]") + 10, s.lastIndexOf("[IRFACQISS]")));
			napasAcqModel.setSVFBNBNP(s.substring(s.indexOf("[IRFACQISS]") + 11, s.lastIndexOf("[IRFACQBNB]")));
			napasAcqModel.setIRFBNBISS(s.substring(s.indexOf("[IRFACQBNB]") + 11, s.lastIndexOf("[SVFBNBNP]")));
			napasAcqModel.setIRFBNBACQ(s.substring(s.indexOf("[SVFBNBNP]") + 10, s.lastIndexOf("[IRFBNBISS]")));
			napasAcqModel.setIRFBNBISS(s.substring(s.indexOf("[IRFBNBISS]") + 11, s.lastIndexOf("[IRFBNBACQ]")));
			napasAcqModel.setIRFBNBACQ(s.substring(s.indexOf("[IRFBNBACQ]") + 11, s.lastIndexOf("[F37]")));
			napasAcqModel.setF37(s.substring(s.indexOf("[F37]") + 5, s.lastIndexOf("[F38]")));
			napasAcqModel.setF38(s.substring(s.indexOf("[F38]") + 5, s.lastIndexOf("[TRN]")));
			napasAcqModel.setTRN(s.substring(s.indexOf("[TRN]") + 5, s.lastIndexOf("[RRC]")));
			napasAcqModel.setRRC(s.substring(s.indexOf("[RRC]") + 5, s.lastIndexOf("[RSV1]")));
			napasAcqModel.setRSV1(s.substring(s.indexOf("[RSV1]") + 6, s.lastIndexOf("[RSV2]")));
			napasAcqModel.setRSV2(s.substring(s.indexOf("[RSV2]") + 6, s.lastIndexOf("[RSV3]")));
			napasAcqModel.setRSV3(s.substring(s.indexOf("[RSV3]") + 6, s.lastIndexOf("[CSR]")));
			napasAcqModel.setCSR(s.substring(s.indexOf("[CSR]") + 5));
			models.add(napasAcqModel);
		});

		return models;
	}

	public static List<String> readFile(String file) {
		List<String> strings = new ArrayList<>();
		// read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(file))) {

			stream.forEach(s -> strings.add(s));
			stream.close();
		} catch (IOException e) {
			System.out.println("Can not read file.");
			e.printStackTrace();
		}
		return strings;
	}

	private static boolean isCreateFile(String pathFile) {
		try {
			Path pathToFile = Paths.get(pathFile);
			if (!Files.exists(pathToFile.getParent()))
				Files.createDirectories(pathToFile.getParent());
			if (!Files.exists(pathToFile))
				Files.createFile(pathToFile);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private static String timeWrite() {
		// Get current date time
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
		return now.format(formatter);
	}
	
	

}
