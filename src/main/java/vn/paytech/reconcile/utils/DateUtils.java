package vn.paytech.reconcile.utils;

import org.springframework.stereotype.Component;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class DateUtils {
    public static final String patternDf = "yyyy-MM-dd";
    public static SimpleDateFormat df = new SimpleDateFormat(patternDf);
    public static final String reportPattern = "dd-MM-yyyy";
    public static SimpleDateFormat reportFormat = new SimpleDateFormat(reportPattern);
    public static final String dayPattern = "dd";
    public static SimpleDateFormat dayFormat = new SimpleDateFormat(dayPattern);
    public static final String monthPattern = "MM";
    public static SimpleDateFormat monthFormat = new SimpleDateFormat(monthPattern);
    public static final String yearPattern = "yyyy";
    public static SimpleDateFormat yearFormat = new SimpleDateFormat(yearPattern);
    public static final String monthFilePattern = "yyyyMM";
    public static SimpleDateFormat monthFileFormat = new SimpleDateFormat(monthFilePattern);

    public Date getDateFromString(String dateString) {
        Date date = null;
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public Time convertTimeString(String time) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Time d = null;
        try {
            d = new Time(dateFormat.parse(time).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public long timeToDailyLong(String dt) {
        long tmp = 0;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            tmp = df.parse(dt).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tmp;
    }

    public long timeLongToHour(long timeLong) {
        long tmp = timeLong % (60 * 60 * 1000);
        return timeLong - tmp;
    }

    public String getDateSql(long dt) {

        Date date = new Date(dt);
        return df.format(date);
    }

    public static String getFirstDayOfLastMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DAY_OF_MONTH, 1);
        return reportFormat.format(c.getTime());
    }

    public static String getFirstDayOfThisMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        return reportFormat.format(c.getTime());
    }

    public static String getLastDayOfLastMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        return reportFormat.format(c.getTime());
    }

    public static String getLastMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        return monthFileFormat.format(c.getTime());
    }

    public static String getThisDay() {
        return dayFormat.format(System.currentTimeMillis());
    }

    public static String getThisDayOfMonth() {
        return reportFormat.format(System.currentTimeMillis());
    }

    public static String getThisMonth() {
        return monthFormat.format(System.currentTimeMillis());
    }

    public static String getThisMonthOfYear() {
        return monthFileFormat.format(System.currentTimeMillis());
    }

    public static String getThisYear() {
        return yearFormat.format(System.currentTimeMillis());
    }

    public static List<String> getListDate(String fromDate, String toDate) {

        List<String> lst = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = df.parse(fromDate);
            Date endDate = df.parse(toDate);
            c.setTime(endDate);
            c.add(Calendar.DAY_OF_MONTH, 1);
            calendar.setTime(date);
            while (calendar.getTime().before(c.getTime())) {
                lst.add(df.format(calendar.getTime()));
                calendar.add(Calendar.DAY_OF_MONTH, 1);

            }
        } catch (ParseException e) {
            System.out.println("error get list date from_date =" + fromDate + " to_date=" + toDate);
            e.printStackTrace();
        }
        return lst;
    }
}
