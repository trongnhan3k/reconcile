package vn.paytech.reconcile.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * @author GiangLV
 *
 */

@Service
public class BeanUtils implements ApplicationContextAware {
	private static final Logger log4j = LoggerFactory.getLogger(BeanUtils.class);
	private static ApplicationContext context;

	/**
	 * getContext - get current context
	 * 
	 * @return type of org.springframework.context.ApplicationContext
	 */
	public static ApplicationContext getContext() {
		return context;
	}

	/**
	 * setContext - set current context
	 * 
	 * @param context
	 *            type of org.springframework.context.ApplicationContext
	 */
	public static void setContext(ApplicationContext context) {
		BeanUtils.context = context;
	}

	/**
	 * setContext - set current context Override
	 * 
	 * @param context
	 *            type of org.springframework.context.ApplicationContext
	 */
	@Override
	public void setApplicationContext(ApplicationContext context) {
		setContext(context);
	}

	/**
	 * getBean - get instance of spring by class
	 * 
	 * @param beanClass
	 *            type of Class
	 * @param <T>
	 *            This is the type parameter
	 * @return type of T
	 */
	public static <T> T getBean(Class<T> beanClass) {
		if (getContext() == null) {
			throw new RuntimeException(beanClass.getName());
		}
		return getContext().getBean(beanClass);
	}

	/**
	 * getBean - get instance of spring by class
	 * 
	 * @param beanClass
	 *            type of Class
	 * @param clazz
	 *            type of T
	 * @param <T>
	 *            This is the type parameter
	 * @return type of T
	 */
	public static <T> T getBean(Class<T> beanClass, T clazz) {
		if (clazz != null) {
			return clazz;
		}
		log4j.info("getBean by class: [{}]", beanClass.getCanonicalName());
		return getBean(beanClass);
	}

	/**
	 * getBean - get instance of spring by name
	 * 
	 * @param beanClass
	 *            type of Class
	 * @param <T>
	 *            This is the type parameter
	 * @return type of Object
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String beanClass) {
		if (getContext() == null) {
			throw new RuntimeException(beanClass);
		}
		return (T) getContext().getBean(beanClass);
	}

	/**
	 * getBean - get instance of spring by name
	 * 
	 * @param beanClass
	 *            type of Class
	 * @param clazz
	 *            type of T
	 * @param <T>
	 *            This is the type parameter
	 * @return type of Object
	 */
	public static <T> T getBean(String beanClass, T clazz) {
		if (clazz != null) {
			return clazz;
		}
		log4j.info("getBean by name: [{}]", beanClass);
		return getBean(beanClass);
	}
}
