package vn.paytech.reconcile.utils;

import org.apache.log4j.Logger;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import com.jcraft.jsch.Session;



public class SftpFileTransfer {
	private static final Logger log = Logger.getLogger(SftpFileTransfer.class);
	
	public String remoteHost;
	public int port;
	public String username;
	public String password;
	public String localFile;
	public String remoteFile;
	public String localDir;
	public String remoteDir;
	public String knownHostsFileLoc;
    
    //@Test
    public void whenUploadFileUsingJsch_thenSuccess(String slFile) throws JSchException, SftpException {
    	
        ChannelSftp channelSftp = setupJsch();
        log.info("whenUploadFileUsingJsch_thenSuccess localFile: "+localFile);
        log.info("whenUploadFileUsingJsch_thenSuccess remoteFile: " + remoteFile);
        channelSftp.connect();
        channelSftp.put(localFile, remoteFile, ChannelSftp.OVERWRITE);
        channelSftp.exit();
    }

    //@Test
    public void whenDownloadFileUsingJsch_thenSuccess(String tc02File) throws JSchException, SftpException {   	
        ChannelSftp channelSftp = setupJsch();
        //log.info("whenDownloadFileUsingJsch_thenSuccess remoteDir: "+remoteDir + tc02File);
        //log.info("whenDownloadFileUsingJsch_thenSuccess localFile: " + localDir + tc02File);
        channelSftp.connect();
        channelSftp.get(remoteDir + tc02File , localDir + tc02File);
//        //download file XL
//        String xlFile = tc02File.replace("1_TC_ECOM", "1_XL_ECOM");
//        channelSftp.get(remoteDir + xlFile , localDir + xlFile);
        channelSftp.exit();
    }
    
    
    private ChannelSftp setupJsch() throws JSchException {
        JSch jsch = new JSch();
        //log.info("Napas_knownHostsFileLoc: "+ knownHostsFileLoc +" username: "+ username +" remoteHost: "+ remoteHost+" port: "+ port);
        jsch.setKnownHosts(knownHostsFileLoc);
        Session jschSession = jsch.getSession(username, remoteHost, port);
        jschSession.setPassword(password);
        jschSession.setConfig("StrictHostKeyChecking", "no");
        jschSession.connect();
        return (ChannelSftp) jschSession.openChannel("sftp");
    }
}
