package vn.paytech.reconcile.utils;

import vn.paytech.reconcile.config.Constant;

public class EvnUtils {

   

    public String getEvnQh(String customerInfo) {
    	if(customerInfo.isEmpty()) {
    		return Constant.mapQhEvn.get(customerInfo);
    	}
        String qhCode = customerInfo.substring(0, 4);
        if (Constant.mapQhEvn.containsKey(qhCode)) {
            return Constant.mapQhEvn.get(qhCode);
        } else {
            return null;
        }
    }
}
