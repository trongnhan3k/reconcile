package vn.paytech.reconcile.utils;


import javax.xml.bind.DatatypeConverter;

import vn.paytech.reconcile.config.LoadFileProperties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class EncryptDecryptManager {
	
	public static String SignCertificate(String text, String certPathPrivateKey, String passwordKey, String alias) {
		try {
			// Nạp private key từ file
			FileInputStream fis = new FileInputStream(certPathPrivateKey);

			// Tạo private key
			KeyStore keystore = KeyStore.getInstance("PKCS12");
			keystore.load(fis, passwordKey.toCharArray());
			PrivateKey key = (PrivateKey)keystore.getKey(alias, passwordKey.toCharArray());
			Signature s = Signature.getInstance("SHA1withRSA");
			s.initSign(key, new SecureRandom());
			s.update(text.getBytes("UTF-16LE"));
			//byte[] kk = text.getBytes("UTF-16");
			fis.close();
			return DatatypeConverter.printBase64Binary(s.sign());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static boolean VerifyCommand(String strText, String signatureStr, String publicKey) {
		
		String certPath = publicKey;
		FileInputStream fis;
		try {
			fis = new FileInputStream(certPath);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
		    X509Certificate cert = (X509Certificate) cf.generateCertificate(fis);   
			PublicKey pubKey = cert.getPublicKey();
			Signature s = Signature.getInstance("SHA1withRSA");
			s.initVerify(pubKey);
			s.update(strText.getBytes("UTF-16LE"));
			return s.verify(DatatypeConverter.parseBase64Binary(signatureStr));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}
	
	public static String hashMD5(String input) {
		try { 
			
            // Static getInstance method is called with hashing MD5 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
  
            // digest() method is called to calculate message digest 
            //  of an input digest() return array of byte 
            byte[] messageDigest = md.digest(input.getBytes()); 
  
            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, messageDigest); 
  
            // Convert message digest into hex value 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) { 
                hashtext = "0" + hashtext; 
            } 
            //System.out.println(hashtext);
            return hashtext; 
        }  
  
        // For specifying wrong message digest algorithms 
        catch (NoSuchAlgorithmException e) { 
            throw new RuntimeException(e); 
        } 
	}
	
	public static String hashMD5Napas(String src) {
		String secretKey = LoadFileProperties.Napas_SecretKey;
        String strChecksum = hashMD5(src).toLowerCase();
        String str = "";
        secretKey = "5" + secretKey + "5";
        String strKQ = strChecksum;
        if (isNumeric(secretKey)) {
            char[] chars = secretKey.toCharArray();
            for (int i = 0; i < chars.length - 1; i++) {
                str = str + strChecksum.substring(Character.getNumericValue(chars[i]), Character.getNumericValue(chars[i]) + 20 - Character.getNumericValue(chars[i + 1]));
            }
            strKQ = hashMD5(str).toLowerCase();
        }
        return strKQ;
    }
	
	public static boolean verifyMD5Hash(String input, String hash) {
		String checkSum = EncryptDecryptManager.hashMD5(input);
		if(checkSum != hash)
			return false;
		return true;
		
	}
	
	public static boolean isNumeric(String strNum) {
	    if (strNum == null) {
	        return false;
	    }
	    try {
	        double d = Double.parseDouble(strNum);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
}
