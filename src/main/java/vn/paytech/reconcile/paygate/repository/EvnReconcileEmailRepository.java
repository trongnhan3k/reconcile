package vn.paytech.reconcile.paygate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.paytech.reconcile.paygate.entity.EvnReconcileEmail;

import java.util.List;

@Repository
public interface EvnReconcileEmailRepository extends JpaRepository<EvnReconcileEmail, Long> {
	
	@Query(value = "select s from EvnReconcileEmail s where qhName = :qhName or companyName = 'ICOM'")
    List<EvnReconcileEmail> findByQhName(String qhName);
}
