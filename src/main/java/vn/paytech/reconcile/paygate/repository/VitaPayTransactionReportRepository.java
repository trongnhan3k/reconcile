package vn.paytech.reconcile.paygate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.paytech.reconcile.paygate.entity.VitaPayTransactionReport;

import javax.transaction.Transactional;

@Repository
public interface VitaPayTransactionReportRepository extends JpaRepository<VitaPayTransactionReport, String> {

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM bank_gateway.vitapay_transaction_report s " +
            "where DATE(s.CREATE_DATE) >= DATE(:fromDate) " +
            "AND DATE(s.CREATE_DATE) < DATE_ADD(:toDate, interval 1 DAY)", nativeQuery = true)
    void deleteData(String fromDate, String toDate);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM bank_gateway.vitapay_transaction_report s " +
            "where DATE(s.CREATE_DATE) >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) " +
            "AND DATE(s.CREATE_DATE) < CURDATE()", nativeQuery = true)
    void deleteData();
}
