package vn.paytech.reconcile.paygate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.paytech.reconcile.paygate.entity.BankTransaction;

import java.util.List;

@Repository
public interface BankTransactionRepository extends JpaRepository<BankTransaction, Long> {
    @Query("select s from BankTransaction s where s.transId = ?1")
    BankTransaction findByTransId(String transId);

    @Query(value = "SELECT * FROM tb_bank_transaction "
            + "where str_merchant_name = :merchantCode and cast(str_ipn_respcode as unsigned integer) = 0 and str_status = 'APPROVED' and "
            + "DATE(dat_ipn_resptime) > DATE_SUB(CURDATE(), INTERVAL 2 DAY) AND DATE(dat_ipn_resptime) < CURDATE() "
            + "order by dat_created asc", nativeQuery = true)
    List<BankTransaction> findAllByMerchantNameAndDate(String merchantCode);

    @Query(value = "SELECT * FROM tb_bank_transaction "
            + "where str_merchant_name = :merchantCode and cast(str_ipn_respcode as unsigned integer) = 0 and str_status = 'APPROVED' "
            + " and DATE(dat_created) >= DATE(:fromDate) " +
            "and DATE(dat_created) <= DATE(:toDate) order by dat_created asc", nativeQuery = true)
    List<BankTransaction> findAllByMerchantName(String merchantCode, String fromDate, String toDate);

    @Query("select s from BankTransaction s where s.ipnRespTxn = ?1")
    BankTransaction findByIpnRespTxn(String ipnRespTxn);

    @Query(value = "select 'PAYMENT_GATEWAY' PAYMENT_METHOD, DATE(dat_req_time) create_Date" +
            ", SUM(NUM_AMOUNT) TOTAL_AMOUNT, COUNT(*) TOTAL_TRANS " +
            ", s.str_status INT_STATUS " +
            ", s.str_merchant_name str_channel, 'PAYMENT' str_refcode, s.STR_BANK_CODE STR_BANKCODE, cast(s.str_ipn_respcode as unsigned integer) STR_IPN_RESPCODE " +
            "from tb_bank_transaction s " +
            "where DATE(s.dat_req_time) >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND DATE(dat_req_time) < CURDATE() " +
            "and s.str_link_type = 'inittrans' " +
            "GROUP BY INT_STATUS, str_channel, str_refcode, STR_BANKCODE, DATE(dat_req_time), cast(s.str_ipn_respcode as unsigned integer) "
            , nativeQuery = true)
    List<Object[]> lstTransLastDay();

    @Query(value = "select 'PAYMENT_GATEWAY' PAYMENT_METHOD, DATE(dat_req_time) create_Date" +
            ", SUM(NUM_AMOUNT) TOTAL_AMOUNT, COUNT(*) TOTAL_TRANS " +
            ", s.str_status INT_STATUS " +
            ", s.str_merchant_name str_channel, 'PAYMENT' str_refcode, s.STR_BANK_CODE STR_BANKCODE, cast(s.str_ipn_respcode as unsigned integer) STR_IPN_RESPCODE " +
            "from tb_bank_transaction s " +
            "where DATE(s.dat_req_time) >= DATE(:fromDate) AND DATE(dat_req_time) < DATE_ADD(:toDate, interval 1 DAY) " +
            "and s.str_link_type = 'inittrans' " +
            "GROUP BY INT_STATUS, str_channel, str_refcode, STR_BANKCODE, DATE(dat_req_time), cast(s.str_ipn_respcode as unsigned integer) "
            , nativeQuery = true)
    List<Object[]> lstTransLastDay(String fromDate, String toDate);

    @Query(value = "SELECT * FROM tb_bank_transaction " +
            "where  cast(str_ipn_respcode as unsigned integer) = 0 and str_status = 'APPROVED' and " +
            "DATE(dat_ipn_resptime) = CURDATE() and str_link_type = 'inittrans' " +
            "order by dat_created asc", nativeQuery = true)
    List<BankTransaction> findAllByCurrentDate();


}
