package vn.paytech.reconcile.paygate.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "VITA_PAY_EVN_RECONCILE_EMAIL_LIST")
@Getter
@Setter
public class EvnReconcileEmail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "QH_NAME")
    private String qhName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "IS_CC")
    private Integer isCc;

}
