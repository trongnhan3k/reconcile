package vn.paytech.reconcile.paygate.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_bank_transaction")
@Getter
@Setter
public class BankTransaction {
	
	@Column(name = "str_merchant_name")
	private String merchantName;
	
	@Column(name = "str_merchant_trans_id")
	private String merchanTransId;
	
	@Column(name = "str_tran_date")
	private String tranDate;
	
	@Id
	@Column(name = "str_trans_id")
	private String transId;
	
	@Column(name = "str_trans_desc")
	private String transDesc;
	
	@Column(name = "num_amount")
	private double numAmount;
	
	@Column(name = "str_curr")
	private String strCurr;
	
	@Column(name = "str_payer_id")
	private String payerId;
	
	@Column(name = "str_payer_name")
	private String payerName;
	
	@Column(name = "str_payer_address")
	private String payerAddress;
	
	@Column(name = "str_type")
	private String type;
	
	@Column(name = "str_customer_id")
	private String customerId;
	
	@Column(name = "str_customer_name")
	private String customerName;
	
	@Column(name = "str_issue_date")
	private String issueDate;
	
	@Column(name = "str_channel_id")
	private String channelId;
	
	@Column(name = "str_link_type")
	private String linkType;
	
	@Column(name = "str_otp_number")
	private String otpNumber;
	
	@Column(name = "str_more_info")
	private String moreInfo;
	
	@Column(name = "str_secure_code")
	private String secureCode;
	
	@Column(name = "dat_req_time")
	private Instant reqTime;
	
	@Column(name = "dat_res_time")
	private Instant resTime;
	
	@Column(name = "str_res_response_code")
	private String resResponseCode;
	
	@Column(name = "str_res_response_txncode")
	private String resResponseTxnCode;
	
	@Column(name = "str_res_redirect_url", length = 1000)
	private String resRedirectUrl;
	
	@Column(name = "str_res_more_info")
	private String resMoreInfo;
	
	@Column(name = "str_bank_code")
	private String bankCode;
	
	@Column(name = "str_ipn_respcode")
	private String ipnRespcode;
	
	@Column(name = "str_ipn_resptxn")
	private String ipnRespTxn;

	@Column(name = "dat_ipn_resptime")
	private Instant ipnResptime;
	
	@Column(name = "dat_created")
	private Instant created;
	
	@Column(name = "dat_update")
	private Instant update;
	
	@Column(name = "str_username")
	private String username;
	
	public BankTransaction() {
		super();
		this.created = Instant.now();
		this.update = Instant.now();
		// TODO Auto-generated constructor stub
	}

	
}
