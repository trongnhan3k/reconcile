package vn.paytech.reconcile.paygate.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "VITAPAY_TRANSACTION_REPORT")
@Getter
@Setter
public class VitaPayTransactionReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "PAYMENT_METHOD")
    private String paymentMethod;

    @Column(name = "CREATE_DATE")
    private String createDate;

    @Column(name = "TOTAL_AMOUNT")
    private Double totalAmount;

    @Column(name = "TOTAL_TRANS")
    private Integer totalTrans;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "CHANNEL")
    private String channel;

    @Column(name = "REFCODE")
    private String refCode;

    @Column(name = "BANKCODE")
    private String bankCode;
}
