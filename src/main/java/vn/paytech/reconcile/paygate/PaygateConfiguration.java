package vn.paytech.reconcile.paygate;

import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "vn.paytech.reconcile.paygate", entityManagerFactoryRef = "paygateEntityManager", transactionManagerRef = "paygateTransactionManager")
public class PaygateConfiguration {
	@Autowired
	private Environment env;

	public PaygateConfiguration() {
		super();
	}

	@Bean(name = "paygateDatasource")
	@ConfigurationProperties(prefix = "spring.paygate-datasource")
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		System.out.println(env.getProperty("spring.paygate-datasource.driver-class-name"));
		dataSource.setDriverClassName(env.getProperty("spring.paygate-datasource.driver-class-name"));
		dataSource.setUrl(env.getProperty("spring.paygate-datasource.url"));
		dataSource.setUsername(env.getProperty("spring.paygate-datasource.username"));
		dataSource.setPassword(env.getProperty("spring.paygate-datasource.password"));

		return dataSource;
		// return DataSourceBuilder.create().build();
	}

	@Bean(name = "paygateEntityManager")
	public LocalContainerEntityManagerFactoryBean paygateEntityManager(EntityManagerFactoryBuilder builder,
			@Qualifier("paygateDatasource") DataSource dataSource) {
		System.out.println("paygate loading config");
//		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//		em.setDataSource(dataSource());
//		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//		em.setJpaVendorAdapter(vendorAdapter);
//		HashMap<String, Object> properties = new HashMap<>();
//        properties.put("hibernate.use-new-id-generator-mappings",
//          env.getProperty("spring.jpa.hibernate.use-new-id-generator-mappings"));
//        em.setJpaPropertyMap(properties);
//        
//        return em;
		return builder.dataSource(dataSource).packages("vn.paytech.reconcile.paygate.entity").persistenceUnit("paygate")
				.build();
	}

	@Bean(name = "paygateTransactionManager")
	public PlatformTransactionManager paygateTransactionManager(
			@Qualifier("paygateEntityManager") EntityManagerFactory paygateEntityManagerFactory) {
		return new JpaTransactionManager(paygateEntityManagerFactory);
	}

}
