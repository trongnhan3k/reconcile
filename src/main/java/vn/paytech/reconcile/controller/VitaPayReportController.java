package vn.paytech.reconcile.controller;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.paytech.reconcile.config.Constant;
import vn.paytech.reconcile.config.LoadFileProperties;
import vn.paytech.reconcile.ewallet.model.CustomersReport;
import vn.paytech.reconcile.paygate.entity.VitaPayTransactionReport;
import vn.paytech.reconcile.services.*;
import vn.paytech.reconcile.utils.BeanUtils;
import vn.paytech.reconcile.utils.DateUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by nhanvt on 2019/12/17
 */
@Controller
@RequestMapping(value = "/report", produces = "application/json")
public class VitaPayReportController {
    private static final Logger log = Logger.getLogger(VitaPayReportController.class);

    private static final DateFormat dateFormatFile = new SimpleDateFormat("yyyyMM");

    @Autowired
    private TransactionInauthServices transactionInAuthServices;

    @Autowired
    private BankTransactionServices bankTransactionService;

    @Autowired
    private VitaPayTransactionReportServices vitaPayTransactionReportServices;

    @Autowired
    private BankAccountsServices bankAccountsServices;

    @Autowired
    private AccountsServices accountsServices;

    @Autowired
    private CustomersServices customersServices;

    @RequestMapping(value = "/updateData", method = RequestMethod.GET)
    public String updateData(HttpServletRequest request) {
        createBean();
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        if (fromDate == null || toDate == null) {
            return "khong duoc roi";
        }
        String format = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date fd = sdf.parse(fromDate);
            Date td = sdf.parse(toDate);
            if (!fromDate.equals(sdf.format(fd)) || !toDate.equals(sdf.format(td))) {
                return "Sai format";
            }
        } catch (Exception e) {
            return "Loi khong parse duoc ngay";
        }

        vitaPayTransactionReportServices.deleteData(fromDate, toDate);

        List<VitaPayTransactionReport> listResult = new ArrayList<>();

        //save log ewallet
        List<Object[]> transactionInauths = transactionInAuthServices.lstTransLastDay(fromDate, toDate);
        log.info(transactionInauths.size());
        for (Object[] ob : transactionInauths) {
            VitaPayTransactionReport vitaPayTransactionReport = new VitaPayTransactionReport();
            try {
                vitaPayTransactionReport.setPaymentMethod(ob[0].toString());
                Date date = sdf.parse(ob[1].toString());
                DateUtils dateUtils = new DateUtils();
                vitaPayTransactionReport.setCreateDate(dateUtils.getDateSql(date.getTime()));
                vitaPayTransactionReport.setTotalAmount(Double.parseDouble(ob[2].toString()));
                vitaPayTransactionReport.setTotalTrans(Integer.parseInt(ob[3].toString()));
            } catch (Exception e) {
                continue;
            }
            vitaPayTransactionReport.setStatus(ob[4].toString());
            if (ob[5] != null)
                vitaPayTransactionReport.setChannel(ob[5].toString());
            if (ob[6] != null)
                vitaPayTransactionReport.setRefCode(ob[6].toString());
            if (ob[7] != null)
                vitaPayTransactionReport.setBankCode(ob[7].toString());
            listResult.add(vitaPayTransactionReport);
        }

        //save log paymentgate
        List<Object[]> bankTransactions = bankTransactionService.lstTransLastDay(fromDate, toDate);
        log.info(bankTransactions.size());
        for (Object[] ob : bankTransactions) {
            VitaPayTransactionReport vitaPayTransactionReport = new VitaPayTransactionReport();
            vitaPayTransactionReport.setPaymentMethod(ob[0].toString());
            vitaPayTransactionReport.setCreateDate(ob[1].toString());
            vitaPayTransactionReport.setTotalAmount(Double.parseDouble(ob[2].toString()));
            vitaPayTransactionReport.setTotalTrans(Integer.parseInt(ob[3].toString()));
            vitaPayTransactionReport.setStatus(ob[4].toString());
            if (ob[5] != null)
                vitaPayTransactionReport.setChannel(ob[5].toString());
            if (ob[6] != null)
                vitaPayTransactionReport.setRefCode(ob[6].toString());
            if (ob[7] != null)
                vitaPayTransactionReport.setBankCode(ob[7].toString());
            listResult.add(vitaPayTransactionReport);
        }

        vitaPayTransactionReportServices.saveAll(listResult);


        return "OK r day";
    }

    @RequestMapping(value = "/getTransactionsData", method = RequestMethod.GET)
    public String getTransactionsData(HttpServletRequest request) {
        createBean();
        String format = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        List<VitaPayTransactionReport> listResult = new ArrayList<>();
        Gson gson = new Gson();
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        List<Object[]> transactionInauths;
        List<Object[]> bankTransactions;
        if (fromDate != null || toDate != null) {
            try {
                Date fd = sdf.parse(fromDate);
                Date td = sdf.parse(toDate);
                assert fromDate != null;
                if (!fromDate.equals(sdf.format(fd)) || !toDate.equals(sdf.format(td))) {
                    return gson.toJson(listResult);
                }
            } catch (Exception e) {
                return gson.toJson(listResult);
            }
            transactionInauths = transactionInAuthServices.lstTransLastDay(fromDate, toDate);
            bankTransactions = bankTransactionService.lstTransLastDay(fromDate, toDate);
        } else {
            transactionInauths = transactionInAuthServices.lstTransLastDay();
            bankTransactions = bankTransactionService.lstTransLastDay();
        }

        log.info(transactionInauths.size());
        for (Object[] ob : transactionInauths) {
            VitaPayTransactionReport vitaPayTransactionReport = new VitaPayTransactionReport();
            try {
                vitaPayTransactionReport.setPaymentMethod(ob[0].toString());
                Date date = sdf.parse(ob[1].toString());
                DateUtils dateUtils = new DateUtils();
                vitaPayTransactionReport.setCreateDate(dateUtils.getDateSql(date.getTime()));
                vitaPayTransactionReport.setTotalAmount(Double.parseDouble(ob[2].toString()));
                vitaPayTransactionReport.setTotalTrans(Integer.parseInt(ob[3].toString()));
            } catch (Exception e) {
                continue;
            }
            vitaPayTransactionReport.setStatus(ob[4].toString());
            if (ob[5] != null)
                vitaPayTransactionReport.setChannel(ob[5].toString());
            if (ob[6] != null)
                vitaPayTransactionReport.setRefCode(ob[6].toString());
            if (ob[7] != null)
                vitaPayTransactionReport.setBankCode(ob[7].toString());
            listResult.add(vitaPayTransactionReport);
        }

        log.info(bankTransactions.size());
        for (Object[] ob : bankTransactions) {
            VitaPayTransactionReport vitaPayTransactionReport = new VitaPayTransactionReport();
            vitaPayTransactionReport.setPaymentMethod(ob[0].toString());
            vitaPayTransactionReport.setCreateDate(ob[1].toString());
            vitaPayTransactionReport.setTotalAmount(Double.parseDouble(ob[2].toString()));
            vitaPayTransactionReport.setTotalTrans(Integer.parseInt(ob[3].toString()));
            vitaPayTransactionReport.setStatus(ob[4].toString());
            if (ob[8] != null) {
                if (Constant.mapIpnResponse.containsKey(ob[8].toString())) {
                    vitaPayTransactionReport.setStatus("SUCCESS");
                }
            }
            if (ob[5] != null)
                vitaPayTransactionReport.setChannel(ob[5].toString());
            if (ob[6] != null)
                vitaPayTransactionReport.setRefCode(ob[6].toString());
            if (ob[7] != null)
                vitaPayTransactionReport.setBankCode(ob[7].toString());
            listResult.add(vitaPayTransactionReport);
        }

        return gson.toJson(listResult);
    }

    @RequestMapping(value = "/getUsersData", method = RequestMethod.GET)
    public String getUsersData(HttpServletRequest request) {
        createBean();
        String format = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        List<CustomersReport> listResult = new ArrayList<>();
        Gson gson = new Gson();
        String fromDate = request.getParameter("fromDate");
        String toDate = request.getParameter("toDate");
        List<Object[]> listCustomers;
        if (fromDate != null || toDate != null) {
            try {
                Date fd = sdf.parse(fromDate);
                Date td = sdf.parse(toDate);
                assert fromDate != null;
                if (!fromDate.equals(sdf.format(fd)) || !toDate.equals(sdf.format(td))) {
                    return gson.toJson(listResult);
                }
            } catch (Exception e) {
                return gson.toJson(listResult);
            }
            listCustomers = customersServices.lstNewUsersByDay(fromDate, toDate);
        } else {
            listCustomers = customersServices.lstNewUsersLastDay();
        }

        log.info(listCustomers.size());
        for (Object[] ob : listCustomers) {
            CustomersReport customersReport = new CustomersReport();
            try {
                Date date = sdf.parse(ob[2].toString());
                DateUtils dateUtils = new DateUtils();
                customersReport.setCreateDate(dateUtils.getDateSql(date.getTime()));
                customersReport.setStatus(Integer.parseInt(ob[1].toString()));
                customersReport.setNewUsers(Integer.parseInt(ob[0].toString()));
            } catch (Exception e) {
                continue;
            }
            listResult.add(customersReport);
        }

        return gson.toJson(listResult);
    }

    @RequestMapping(value = "/getLinkedBankUsersData", method = RequestMethod.GET)
    public String getLinkedBankUsersData() {
        createBean();
        Gson gson = new Gson();
        return gson.toJson(bankAccountsServices.totalLinkedBankUsers());
    }

    @RequestMapping(value = "/getTotalAccountBalanceAmount", method = RequestMethod.GET)
    public String getTotalBalanceAmount() {
        createBean();
        Gson gson = new Gson();
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return gson.toJson(formatter.format(accountsServices.totalBalanceAmount()));
    }

    @RequestMapping(value = "/getFileReconcileEvnHnMonthly", method = RequestMethod.GET)
    public InputStreamResource getFileReconcileEvnHnMonthly(HttpServletRequest request, HttpServletResponse response) {
        String qh = request.getParameter("qh");
        String month = request.getParameter("dt");
        String filePath = String.format("%s%s/%s/%s", LoadFileProperties.ReportDir, "EPOINT",
                month, qh + ".xlsx");
        InputStreamResource resource = null;
        try {
            resource = new InputStreamResource(new FileInputStream(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return resource;
    }

    private void createBean() {
        // neu null thi khoi tao moi
        if (transactionInAuthServices == null)
            transactionInAuthServices = BeanUtils.getBean(TransactionInauthServices.class);
        if (bankTransactionService == null)
            bankTransactionService = BeanUtils.getBean(BankTransactionServices.class);
        if (vitaPayTransactionReportServices == null)
            vitaPayTransactionReportServices = BeanUtils.getBean(VitaPayTransactionReportServices.class);
        if (customersServices == null)
            customersServices = BeanUtils.getBean(CustomersServices.class);
        if (bankAccountsServices == null)
            bankAccountsServices = BeanUtils.getBean(BankAccountsServices.class);
        if (accountsServices == null)
            accountsServices = BeanUtils.getBean(AccountsServices.class);
    }
}
