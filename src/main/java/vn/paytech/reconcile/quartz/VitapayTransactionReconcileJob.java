package vn.paytech.reconcile.quartz;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.paytech.reconcile.config.Constant;
import vn.paytech.reconcile.config.LoadFileProperties;
import vn.paytech.reconcile.ewallet.entity.TransactionInauth;
import vn.paytech.reconcile.paygate.entity.BankTransaction;
import vn.paytech.reconcile.services.BankTransactionServices;
import vn.paytech.reconcile.services.EmailService;
import vn.paytech.reconcile.services.TransactionInauthServices;
import vn.paytech.reconcile.utils.BeanUtils;
import vn.paytech.reconcile.utils.EvnUtils;
import vn.paytech.reconcile.utils.Utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@DisallowConcurrentExecution
public class VitapayTransactionReconcileJob implements Job {
    private static final Logger log = Logger.getLogger(VitapayTransactionReconcileJob.class);

    @Autowired
    private TransactionInauthServices transactionInAuthServices;

    @Autowired
    private EmailService emailService;

    @Autowired
    private BankTransactionServices bankTransactionService;

    @Autowired
    private ObjectMapper objectMapper;

    //PAYMENT_METHOD
    private String VITAPAY = "VITAPAY";
    private String ATM = "ATM";

    //TRANSACTION_TYPE
    private String DEPOSITBYBANK = "DEPOSITBYBANK";
    private String WITHDRAW = "WITHDRAW";
    private String P2PTRANSFER = "P2PTRANSFER";
    private String TOPUP = "TOPUP";
    private String VASTRANSFER = "VASTRANSFER";


    public VitapayTransactionReconcileJob() {

    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormatFile = new SimpleDateFormat("yyyyMMdd");

        String dirPath = String.format("%s%s/", LoadFileProperties.ReportDir, VITAPAY);

        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();

        String CSV_FILE_NAME = dirPath + dateFormatFile.format(date) + ".csv";

        List<String[]> dataLines = new ArrayList<>();
        dataLines.add(LoadFileProperties.HeaderCSV.split(","));

        Map reportTransaction = new LinkedHashMap<String, List<Double>>();

        reportTransaction.put(DEPOSITBYBANK, new ArrayList<Double>());
        reportTransaction.put(WITHDRAW, new ArrayList<Double>());
        reportTransaction.put(P2PTRANSFER, new ArrayList<Double>());
        reportTransaction.put(TOPUP, new ArrayList<Double>());
        reportTransaction.put(VASTRANSFER, new ArrayList<Double>());
        reportTransaction.put(ATM, new ArrayList<Double>());

        try {
            // TODO Auto-generated method stub
            log.info("VitapayTransactionReconcileJob start.....");
            this.createBean();

            List<TransactionInauth> transactionInauths = transactionInAuthServices.lstTransCurrentDate();
            log.info("transactionInauths.size(): " + transactionInauths.size());

            EvnUtils evnUtils = new EvnUtils();
            String moreInfo = "";

            for (int i = 0; i < transactionInauths.size(); i++) {
                TransactionInauth item = transactionInauths.get(i);
                int fee = item.getFeeAmount() + item.getVatAmount();
                String description = item.getDescription();

                if (Utils.isJSONValid(description)) {
                    if (item.getChannel().equalsIgnoreCase("EPOINT")) {
                        Map unStructureData = objectMapper.readValue(description, Map.class);
                        String userIdEVN = (String) unStructureData.get("userId");
                        if (userIdEVN == null || userIdEVN.isEmpty()) {
                            userIdEVN = (String) unStructureData.get("user_id");
                        }
                        moreInfo = evnUtils.getEvnQh(userIdEVN);
                    }
                    description = description.replace(",", ".");
                }
                dataLines.add(new String[]{String.valueOf(item.getAuthDate()), item.getUsername(),
                        String.valueOf(item.getAmount()), String.valueOf(fee), item.getTransactionNo(),
                        String.valueOf(item.getTransactionType()), item.getSenderInfo(), item.getReceiverInfo(),
                        item.getRefCode(), description, item.getBankCode(), item.getChannel(), moreInfo, VITAPAY});

            }

            List<BankTransaction> bankTransactions = bankTransactionService.lstTransCurrentDate();
            log.info("bankTransactions.size(): " + bankTransactions.size());
            moreInfo = "";
            for (int i = 0; i < bankTransactions.size(); i++) {
                BankTransaction item = bankTransactions.get(i);
                int fee = 0;
                String description = item.getMoreInfo();

                if (Utils.isJSONValid(description)) {
                    if (item.getMerchantName().equalsIgnoreCase("EPOINT")) {
                        Map unStructureData = objectMapper.readValue(description, Map.class);
                        String userIdEVN = (String) unStructureData.get("userId");
                        if (userIdEVN == null || userIdEVN.isEmpty()) {
                            userIdEVN = (String) unStructureData.get("user_id");
                        }
                        moreInfo = evnUtils.getEvnQh(userIdEVN);
                    }
                    description = description.replace(",", ".");
                }

                dataLines.add(new String[]{String.valueOf(item.getIpnResptime()), item.getUsername(),
                        String.valueOf(item.getNumAmount()), String.valueOf(fee), item.getTransId(), item.getLinkType(),
                        item.getChannelId(), item.getMerchantName(), item.getIpnRespTxn(), description,
                        item.getBankCode(), item.getMerchantName(), moreInfo, ATM});

            }

            try {
                log.info(new ObjectMapper().writeValueAsString(dataLines));
            } catch (JsonProcessingException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                // e1.printStackTrace();

            }
            this.writeToCsvFile(CSV_FILE_NAME, dataLines);

            //

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        dateFormatFile = new SimpleDateFormat("dd-MM-yyyy");
        String[] cc = new String[]{};
        List<String> files = new ArrayList<String>();
        files.add(CSV_FILE_NAME);
//        try {
        String sDate = dateFormatFile.format(date);
        int totalTransactions = dataLines.size() - 1;
        int i = 0;

        for (String[] line : dataLines) {
            i++;
            if (i == 1)
                continue;

            Double amount = Double.parseDouble(line[2]);
            String mapKey = "";
            if (line[line.length - 1].equalsIgnoreCase(ATM)) {
                mapKey = ATM;
            } else {
                mapKey = line[8];
            }

            List<Double> dataItem = (List<Double>) reportTransaction.get(mapKey);
            dataItem.add(amount);
            reportTransaction.replace(mapKey, dataItem);

        }

        List<Double> depositTotal = (List<Double>) reportTransaction.get(DEPOSITBYBANK);
        List<Double> withdrawTotal = (List<Double>) reportTransaction.get(WITHDRAW);
        List<Double> p2pTransferTotal = (List<Double>) reportTransaction.get(P2PTRANSFER);
        List<Double> topupTotal = (List<Double>) reportTransaction.get(TOPUP);
        List<Double> vasTransferTotal = (List<Double>) reportTransaction.get(VASTRANSFER);
        List<Double> atmTotal = (List<Double>) reportTransaction.get(ATM);

        Double deposit = depositTotal.stream().mapToDouble(Double::doubleValue).sum();
        Double withdraw = withdrawTotal.stream().mapToDouble(Double::doubleValue).sum();
        Double p2pTransfer = p2pTransferTotal.stream().mapToDouble(Double::doubleValue).sum();
        Double topup = topupTotal.stream().mapToDouble(Double::doubleValue).sum();
        Double vasTransfer = vasTransferTotal.stream().mapToDouble(Double::doubleValue).sum();
        Double atm = atmTotal.stream().mapToDouble(Double::doubleValue).sum();

        Double total = deposit + withdraw + p2pTransfer + vasTransfer + atm;

        String body = String.format(Constant.PAYTECH_RECONCILE_TEXT, sDate, totalTransactions,
                depositTotal.size(), deposit,
                withdrawTotal.size(), withdraw,
                p2pTransferTotal.size(), p2pTransfer,
                topupTotal.size(), topup,
                vasTransferTotal.size(), vasTransfer,
                atmTotal.size(), atm,
                total
        );

        emailService.sendEmail(files.toArray(new String[files.size()]), body, String.format(Constant.PAYTECH_RECONCILE_SUBJECT, sDate)
                , LoadFileProperties.EmailLists.split(","), cc, null);
//            emailService.sendMessageWithAttachment(LoadFileProperties.EmailLists.split(","), cc,
//                    String.format(Constant.PAYTECH_RECONCILE_SUBJECT, sDate), body, files);


//        } catch (JsonProcessingException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

    }


    private String convertToCSV(String[] data) {
        return Stream.of(data).map(this::escapeSpecialCharacters).collect(Collectors.joining(","));
    }

    private String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    private void writeToCsvFile(String CSV_FILE_NAME, List<String[]> dataLines) throws IOException {
        try {
            FileWriter csv = new FileWriter(CSV_FILE_NAME);
            CSVWriter writer = new CSVWriter(csv, ',', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            writer.writeAll(dataLines);
            csv.close();
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

    private void createBean() {
        // neu null thi khoi tao moi
        if (transactionInAuthServices == null)
            transactionInAuthServices = BeanUtils.getBean(TransactionInauthServices.class);
        if (emailService == null)
            emailService = BeanUtils.getBean(EmailService.class);
        if (bankTransactionService == null)
            bankTransactionService = BeanUtils.getBean(BankTransactionServices.class);
        if (objectMapper == null)
            objectMapper = BeanUtils.getBean(ObjectMapper.class);
    }

}
