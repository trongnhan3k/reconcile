package vn.paytech.reconcile.quartz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import vn.paytech.reconcile.config.LoadFileProperties;
import vn.paytech.reconcile.napas.model.NapasAcqModel;
import vn.paytech.reconcile.paygate.entity.BankTransaction;
import vn.paytech.reconcile.services.BankTransactionServices;
import vn.paytech.reconcile.utils.BeanUtils;
import vn.paytech.reconcile.utils.FileUtils;
import vn.paytech.reconcile.utils.PGPas;
import vn.paytech.reconcile.utils.SftpFileTransfer;

@Component
@DisallowConcurrentExecution
public class WhiteLabelNapasReconcileJob implements Job {

	private static final Logger log = Logger.getLogger(WhiteLabelNapasReconcileJob.class);

	@Autowired
	private BankTransactionServices bankTransactionServices;

	private static String SERVICENAME = "PAYTECHWL1,PAYTECHWL2,PAYTECHWL3";

	private static String SLFILE = "SL";

	private static String FILEEXT = ".pgp";

	private static String FILETYPE = "ACQ";

	private static String NAPASFILES = "TC,XL";

	private static String XLFILE = "XL";

	public WhiteLabelNapasReconcileJob() {

	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// neu null thi khoi tao moi
		if (bankTransactionServices == null)
			bankTransactionServices = BeanUtils.getBean(BankTransactionServices.class);
		// TODO Auto-generated method stub
		SftpFileTransfer sftp_client = new SftpFileTransfer();
		sftp_client.remoteHost = LoadFileProperties.Napas_remoteHost;
		sftp_client.port = Integer.parseInt(LoadFileProperties.Napas_port);
		sftp_client.username = LoadFileProperties.Napas_username;
		sftp_client.password = LoadFileProperties.Napas_password;
		sftp_client.knownHostsFileLoc = LoadFileProperties.Napas_knownHostsFileLoc;
		sftp_client.remoteFile = LoadFileProperties.Napas_remoteFile;

		sftp_client.remoteDir = LoadFileProperties.Napas_remoteDir;

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormatFile = new SimpleDateFormat("MMddyy");
		String strDate = dateFormatFile.format(date);

		for (String serviceName : SERVICENAME.split(",")) {
			for (String fileName : NAPASFILES.split(",")) {
				String TC02 = LoadFileProperties.pathFile(strDate, FILETYPE, serviceName, fileName);

				log.info(TC02);

				String TC02OutFile = TC02.substring(0, TC02.lastIndexOf(FILEEXT));
				log.info(TC02OutFile);

				PGPas.localDir = LoadFileProperties.Napas_localDir + strDate + "/";
				File dir = new File(PGPas.localDir);
				if (!dir.exists())
					dir.mkdirs();
				try {
					// download file from napas
					sftp_client.localDir = LoadFileProperties.Napas_localDir + strDate + "/";
					sftp_client.whenDownloadFileUsingJsch_thenSuccess(TC02);
					// decrypt data
					PGPas.PGPdecrypt(TC02, TC02OutFile);
					if (fileName.equals(XLFILE))
						continue;
					// compare
					String filePath = PGPas.getFilePath(TC02OutFile);
					// compare
					List<NapasAcqModel> napasAcqModels = this.compareData(filePath);
					// write SL
					FileUtils.writeFileCompare(strDate, napasAcqModels, serviceName, SLFILE);
					// encrypt data
					String slFile = LoadFileProperties.pathFile(strDate, FILETYPE, serviceName, SLFILE);
					PGPas.PGPencrypt(slFile.substring(0, slFile.indexOf(FILEEXT)), slFile);
			//upload
					sftp_client.localFile = LoadFileProperties.Napas_localDir + strDate + "/" + slFile;
					sftp_client.remoteFile = LoadFileProperties.Napas_remoteFile + slFile;
					sftp_client.whenUploadFileUsingJsch_thenSuccess(slFile);
				} catch (JSchException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SftpException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					log.info(e.getMessage());

					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {

				}
			}
		}

	}

	private List<NapasAcqModel> compareData(String filePath) throws JsonProcessingException {
		List<NapasAcqModel> napasAcqModels = FileUtils.readFileNapas(filePath);
		for (Iterator<NapasAcqModel> model = napasAcqModels.iterator(); model.hasNext();) {
			NapasAcqModel napasAcqModel = model.next();
			log.info(napasAcqModel.getF37().trim());
			BankTransaction bankTransaction = bankTransactionServices.transByIpnRespTxn(napasAcqModel.getF37().trim());
			
			if (bankTransaction == null) {
				log.info("Khong ton tai napas txn: " + napasAcqModel.getF37());
				continue;
			}
			log.info(new ObjectMapper().writeValueAsString(bankTransaction));
			Double napasAmount = Double.valueOf(napasAcqModel.getF4());
			Double paytechAmount = Double.valueOf(bankTransaction.getNumAmount()*100);
			int test = Double.compare(napasAmount, paytechAmount);
			if (Double.compare(napasAmount, paytechAmount) != 0
					|| Integer.parseInt(bankTransaction.getIpnRespcode()) != 0) {
				continue;
			}
			model.remove();
		}
		return napasAcqModels;

	}

}
