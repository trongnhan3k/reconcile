package vn.paytech.reconcile.quartz;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import vn.paytech.reconcile.paygate.entity.VitaPayTransactionReport;
import vn.paytech.reconcile.services.BankTransactionServices;
import vn.paytech.reconcile.services.TransactionInauthServices;
import vn.paytech.reconcile.services.VitaPayTransactionReportServices;
import vn.paytech.reconcile.utils.BeanUtils;

import java.util.ArrayList;
import java.util.List;

@DisallowConcurrentExecution
public class VitaPayEwalletTransactionReportJob implements Job {
    private static final Logger log = Logger.getLogger(VitaPayEwalletTransactionReportJob.class);

    @Autowired
    private TransactionInauthServices transactionInAuthServices;

    @Autowired
    private BankTransactionServices bankTransactionService;

    @Autowired
    private VitaPayTransactionReportServices vitaPayTransactionReportServices;

    @Override
    public void execute(JobExecutionContext context) {
        try {
            // TODO Auto-generated method stub
            log.info("VitaPay transaction report daily batch start.....");

            this.createBean();

            getData();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    private void getData() {
        vitaPayTransactionReportServices.deleteData();
        List<VitaPayTransactionReport> listResult = new ArrayList<>();

        //save log ewallet
        List<Object[]> transactionInauths = transactionInAuthServices.lstTransLastDay();
        log.info(transactionInauths.size());
        for (Object[] ob : transactionInauths) {
            VitaPayTransactionReport vitaPayTransactionReport = new VitaPayTransactionReport();
            vitaPayTransactionReport.setPaymentMethod(ob[0].toString());
            vitaPayTransactionReport.setCreateDate(ob[1].toString());
            vitaPayTransactionReport.setTotalAmount(Double.parseDouble(ob[2].toString()));
            vitaPayTransactionReport.setTotalTrans(Integer.parseInt(ob[3].toString()));
            vitaPayTransactionReport.setStatus(ob[4].toString());
            if (ob[5] != null)
                vitaPayTransactionReport.setChannel(ob[5].toString());
            if (ob[6] != null)
                vitaPayTransactionReport.setRefCode(ob[6].toString());
            if (ob[7] != null)
                vitaPayTransactionReport.setBankCode(ob[7].toString());
            listResult.add(vitaPayTransactionReport);
        }

        //save log paymentgate
        List<Object[]> bankTransactions = bankTransactionService.lstTransLastDay();
        log.info(bankTransactions.size());
        for (Object[] ob : bankTransactions) {
            VitaPayTransactionReport vitaPayTransactionReport = new VitaPayTransactionReport();
            vitaPayTransactionReport.setPaymentMethod(ob[0].toString());
            vitaPayTransactionReport.setCreateDate(ob[1].toString());
            vitaPayTransactionReport.setTotalAmount(Double.parseDouble(ob[2].toString()));
            vitaPayTransactionReport.setTotalTrans(Integer.parseInt(ob[3].toString()));
            vitaPayTransactionReport.setStatus(ob[4].toString());
            if (ob[5] != null)
                vitaPayTransactionReport.setChannel(ob[5].toString());
            if (ob[6] != null)
                vitaPayTransactionReport.setRefCode(ob[6].toString());
            if (ob[7] != null)
                vitaPayTransactionReport.setBankCode(ob[7].toString());
            listResult.add(vitaPayTransactionReport);
        }

        vitaPayTransactionReportServices.saveAll(listResult);
    }

    private void createBean() {
        // neu null thi khoi tao moi
        if (transactionInAuthServices == null)
            transactionInAuthServices = BeanUtils.getBean(TransactionInauthServices.class);
        if (bankTransactionService == null)
            bankTransactionService = BeanUtils.getBean(BankTransactionServices.class);
        if (vitaPayTransactionReportServices == null)
            vitaPayTransactionReportServices = BeanUtils.getBean(VitaPayTransactionReportServices.class);
    }

}
