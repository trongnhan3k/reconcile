package vn.paytech.reconcile.quartz;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import vn.paytech.reconcile.config.Constant;
import vn.paytech.reconcile.config.LoadFileProperties;
import vn.paytech.reconcile.ewallet.entity.TransactionInauth;
import vn.paytech.reconcile.model.BBXN_DT_Phi;
import vn.paytech.reconcile.model.BangKe;
import vn.paytech.reconcile.paygate.entity.BankTransaction;
import vn.paytech.reconcile.services.BankTransactionServices;
import vn.paytech.reconcile.services.EmailServiceOld;
import vn.paytech.reconcile.services.EvnReconcileEmailServices;
import vn.paytech.reconcile.services.TransactionInauthServices;
import vn.paytech.reconcile.utils.BeanUtils;
import vn.paytech.reconcile.utils.DateUtils;
import vn.paytech.reconcile.utils.EvnUtils;
import vn.paytech.reconcile.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@DisallowConcurrentExecution
public class EVNHaNoiByMonthReconcileJob implements Job {
    private static final Logger log = Logger.getLogger(EVNHaNoiByMonthReconcileJob.class);

    @Autowired
    private TransactionInauthServices transactionInAuthServices;

    @Autowired
    private BankTransactionServices bankTransactionService;

    @Autowired
    private EmailServiceOld emailServiceOld;

    @Autowired
    private EvnReconcileEmailServices evnReconcileEmailServices;

    @Autowired
    private ObjectMapper mapper;

    private static final String MERCHANTCODE = "EPOINT";
    private static final String prefix = "auto_payment_";
    private static final String AUTO = "AUTO";
    private static String TEMPLATE = "";
    private static final DateFormat dateFormatFile = new SimpleDateFormat("yyyyMM");
    private static final DateFormat dateMysqlFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat inputATMFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private static final DateFormat inputEWalletFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public EVNHaNoiByMonthReconcileJob() {

    }

    @Override
    public void execute(JobExecutionContext context) {
        try {
            // TODO Auto-generated method stub
            log.info("EVN reconcile start.....");

            Map<String, List<String[]>> transactions;
            Iterator iterator = Constant.mapQhEvn.entrySet().iterator();
            if (DateUtils.getThisDay().equals("21")) {
                transactions = this.getData(DateUtils.getFirstDayOfThisMonth(), "20-12-2020");
                while (iterator.hasNext()) {
                    Map.Entry pair = (Map.Entry) iterator.next();
                    this.createExcelFile((String) pair.getValue()
                            , transactions.get(pair.getValue())
                            , DateUtils.getFirstDayOfThisMonth()
                            , "20-12-2020");
                    // iterator.remove(); // avoids a ConcurrentModificationException
                }
            } else {
                transactions = this.getData(DateUtils.getFirstDayOfLastMonth(), DateUtils.getLastDayOfLastMonth());
                while (iterator.hasNext()) {
                    Map.Entry pair = (Map.Entry) iterator.next();
                    this.createExcelFile((String) pair.getValue()
                            , transactions.get(pair.getValue())
                            , DateUtils.getFirstDayOfLastMonth()
                            , DateUtils.getLastDayOfLastMonth());
                    // iterator.remove(); // avoids a ConcurrentModificationException
                }
            }
//            if (DateUtils.getThisMonth().equals("12")) {
//                transactions = this.getData();
//            }


            //

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    public Map<String, List<String[]>> getData(String fromDate, String toDate) throws JsonProcessingException, ParseException {
        this.createBean();
        Map<String, List<String[]>> results = new LinkedHashMap<>();
        String[] dataRow;
        List<TransactionInauth> transactionInauths = transactionInAuthServices.lstPaymentTransByMerchant(MERCHANTCODE, fromDate, toDate);
        log.info(transactionInauths.size());
        EvnUtils evnUtils = new EvnUtils();
        for (TransactionInauth transactionInauth : transactionInauths) {
            Map moreInfo = new LinkedHashMap<String, Object>();
            if (Utils.isJSONValid(transactionInauth.getDescription())) {
                moreInfo = mapper.readValue(transactionInauth.getDescription(), Map.class);
            }

            String userId = (String) moreInfo.getOrDefault("userId", "");
            if (userId == null || userId.isEmpty()) {
                userId = (String) moreInfo.getOrDefault("user_id", "");
            }
            String customerName = (String) moreInfo.getOrDefault("userName", "");
            if (customerName == null || customerName.isEmpty()) {
                customerName = (String) moreInfo.getOrDefault("user_name", "");
            }
            String evnQh = evnUtils.getEvnQh(userId);
            String channel = MERCHANTCODE;
            if (transactionInauth.getRefNo().startsWith(prefix)) {
                channel = AUTO;
            }
            dataRow = new String[]{userId, customerName, transactionInauth.getReceiverInfo(), "", "",
                    transactionInauth.getTransactionNo(), String.valueOf(transactionInauth.getAmount()),
                    String.valueOf(transactionInauth.getCreateDate()), channel};

            if (evnQh == null) {
                log.info(mapper.writeValueAsString(dataRow));
                continue;
            }

            List<String[]> data = new ArrayList<>();

            if (results.containsKey(evnQh)) {
                data = results.get(evnQh);
            }
            data.add(dataRow);
            results.put(evnQh, data);

        }

        List<BankTransaction> bankTransactions = bankTransactionService.lstTransByMerchant(MERCHANTCODE
                , dateMysqlFormat.format(DateUtils.reportFormat.parse(fromDate))
                , dateMysqlFormat.format(DateUtils.reportFormat.parse(toDate)));
        log.info(bankTransactions.size());
        for (BankTransaction bankTransaction : bankTransactions) {
            Map moreInfo = new LinkedHashMap<String, Object>();
            if (Utils.isJSONValid(bankTransaction.getMoreInfo())) {
                moreInfo = mapper.readValue(bankTransaction.getMoreInfo(), Map.class);
            }

            String userId = (String) moreInfo.getOrDefault("userId", "");
            String evnQh = evnUtils.getEvnQh(userId);

            dataRow = new String[]{userId, (String) moreInfo.getOrDefault("userName", ""),
                    bankTransaction.getCustomerId(), "", "", bankTransaction.getTransId(),
                    String.valueOf(bankTransaction.getNumAmount()), bankTransaction.getIpnResptime().toString(),
                    MERCHANTCODE};

            if (evnQh == null) {
                log.info(mapper.writeValueAsString(dataRow));
                continue;
            }

            List<String[]> data = new ArrayList<>();

            if (results.containsKey(evnQh)) {
                data = results.get(evnQh);
            }
            data.add(dataRow);
            results.put(evnQh, data);
        }
        return results;
    }

    public void createExcelFile(String fileName, List<String[]> dataRows, String fromDate, String toDate) throws IOException, ParseException {
        TEMPLATE = String.format("%s/%s/templates/month-template.xlsx", LoadFileProperties.ReportDir, MERCHANTCODE);
        log.info(toDate);
        Date a = DateUtils.reportFormat.parse(toDate);
        log.info(a);
        String b = DateUtils.monthFileFormat.format(a);
        log.info(b);
        String pathFile = String.format("%s%s/%s/", LoadFileProperties.ReportDir, MERCHANTCODE,
                b);

        File dir = new File(pathFile);
        if (!dir.exists())
            dir.mkdirs();

        String reconcileFile = pathFile + fileName + ".xlsx";

        File f = new File(reconcileFile);
        if (f.exists() && !f.isDirectory()) {
            f.delete();
        }

        FileUtils.copyFile(new File(TEMPLATE), f);
//        debug(reconcileFile);
        this.writeToExcelFile(fileName, reconcileFile, dataRows, fromDate, toDate);
    }

    private void debug(String reconcileFile) throws IOException {
        File jfile = new File(reconcileFile);
        FileInputStream file = new FileInputStream(jfile);
        Workbook workbook = new XSSFWorkbook(file);
        Sheet sheetBbxnDtPhi = workbook.getSheetAt(0);
//        Map<String, BBXN_DT_Phi> mapDaily = Utils.initMapDaily(DateUtils.getFirstDayOfmonthFile(),
//                DateUtils.getLastDayOfmonthFile());
    }

    private void writeToExcelFile(String fileName, String reconcileFile, List<String[]> dataRows, String fromDate, String toDate) {
        try {
            log.info("file: " + fileName);
            //init
            File jfile = new File(reconcileFile);
            FileInputStream file = new FileInputStream(jfile);
            Workbook workbook = new XSSFWorkbook(file);
            double totalAmount = 0.0;
            double totalFee = 0.0;
            Map<String, BBXN_DT_Phi> mapDaily = Utils.initMapDaily(fromDate, toDate);

            // sheet BBXN_DT_Phi
            //replace template
            Sheet sheetBbxnDtPhi = workbook.getSheetAt(0);
            String a1 = sheetBbxnDtPhi.getRow(4).getCell(0).getStringCellValue()
                    .replaceAll(":fromDate", fromDate)
                    .replaceAll(":toDate", toDate);
            sheetBbxnDtPhi.getRow(4).getCell(0).setCellValue(a1);
            String a2 = sheetBbxnDtPhi.getRow(6).getCell(0).getStringCellValue()
                    .replaceAll(":Qh", fileName);
            sheetBbxnDtPhi.getRow(6).getCell(0).setCellValue(a2);
            String a3 = sheetBbxnDtPhi.getRow(7).getCell(0).getStringCellValue()
                    .replaceAll(":fromDate", fromDate)
                    .replaceAll(":toDate", toDate)
                    .replaceAll(":day", DateUtils.getThisDay())
                    .replaceAll(":month", DateUtils.getThisMonth())
                    .replaceAll(":year", DateUtils.getThisYear());
            sheetBbxnDtPhi.getRow(7).getCell(0).setCellValue(a3);
            String a14 = sheetBbxnDtPhi.getRow(46).getCell(0).getStringCellValue()
                    .replaceAll(":day", DateUtils.getThisDay())
                    .replaceAll(":month", DateUtils.getThisMonth())
                    .replaceAll(":year", DateUtils.getThisYear());
            sheetBbxnDtPhi.getRow(46).getCell(0).setCellValue(a14);
            //caculate data
            if (dataRows != null) {
                for (String[] line : dataRows) {
                    Date date;
                    if (line[7].contains("T")) {
                        date = inputATMFormat.parse(line[7]);
                    } else {
                        date = inputEWalletFormat.parse(line[7]);
                    }
                    String dt = DateUtils.reportFormat.format(date);
                    if (mapDaily.containsKey(dt)) {
                        if (line[8] != null && line[8].equals(AUTO)) {
                            mapDaily.get(dt).setAmountAuto(
                                    mapDaily.get(dt).getAmountAuto() + Double.parseDouble(line[6]));
                            mapDaily.get(dt).setNumberAuto(mapDaily.get(dt).getNumberAuto() + 1);
                            mapDaily.get(dt).setFeeAuto(Constant.EVNHN_FEE_INCLUDEDVAT_AUTO);
                        } else {
                            mapDaily.get(dt).setAmountEpoint(
                                    mapDaily.get(dt).getAmountEpoint() + Double.parseDouble(line[6]));
                            mapDaily.get(dt).setNumberEpoint(mapDaily.get(dt).getNumberEpoint() + 1);
                            mapDaily.get(dt).setFeeEpoint(Constant.EVNHN_FEE_INCLUDEDVAT_EPOINT);
                        }
                    }
                }
            }
            //insert data to excel
            int stt = 1;
            int rowIndexBbxnDtPhi = 12;
            int numberEpoint = 0;
            int numberAuto = 0;
            double amountEpoint = 0.0;
            double amountAuto = 0.0;
            double fee = 0.0;
            for (Map.Entry<String, BBXN_DT_Phi> entry : mapDaily.entrySet()) {
                BBXN_DT_Phi value = entry.getValue();
                totalAmount += value.getAmountAuto() + value.getAmountEpoint();
                totalFee += value.getFeeAuto() + value.getFeeEpoint();
                // create row
                Row row = sheetBbxnDtPhi.getRow(rowIndexBbxnDtPhi);
                // create cell
                // stt
                Cell cell = row.getCell(0);
                cell.setCellValue(stt);
                String dt = entry.getKey();
                row.getCell(1).setCellValue(dt);
                row.getCell(2).setCellValue(value.getNumberEpoint());
                row.getCell(3).setCellValue(value.getNumberAuto());
                row.getCell(4).setCellValue(value.getAmountEpoint());
                row.getCell(5).setCellValue(value.getAmountAuto());
                row.getCell(6).setCellValue(value.getFeeEpoint());
                row.getCell(7).setCellValue(value.getFeeAuto());
                row.getCell(8).setCellValue(value.getNumberEpoint() * value.getFeeEpoint()
                        + value.getNumberAuto() * value.getFeeAuto());
                stt++;
                rowIndexBbxnDtPhi++;
                numberEpoint += value.getNumberEpoint();
                numberAuto += value.getNumberAuto();
                amountEpoint += value.getAmountEpoint();
                amountAuto += value.getAmountAuto();
                fee = fee + Constant.EVNHN_FEE_INCLUDEDVAT_AUTO * value.getNumberAuto()
                        + Constant.EVNHN_FEE_INCLUDEDVAT_EPOINT * value.getNumberEpoint();
            }
            sheetBbxnDtPhi.getRow(43).getCell(2).setCellValue(numberEpoint);
            sheetBbxnDtPhi.getRow(43).getCell(3).setCellValue(numberAuto);
            sheetBbxnDtPhi.getRow(43).getCell(4).setCellValue(amountEpoint);
            sheetBbxnDtPhi.getRow(43).getCell(5).setCellValue(amountAuto);
            sheetBbxnDtPhi.getRow(43).getCell(8).setCellValue(fee);

            // sheet BBDS_EVN
            Sheet sheetBbdsEvn = workbook.getSheetAt(1);
            String a4 = sheetBbdsEvn.getRow(15).getCell(0).getStringCellValue()
                    .replaceAll(":fromDate", fromDate)
                    .replaceAll(":toDate", toDate);
            sheetBbdsEvn.getRow(15).getCell(0).setCellValue(a4);
            String a5 = sheetBbdsEvn.getRow(33).getCell(0).getStringCellValue()
                    .replaceAll(":day", DateUtils.getThisDay())
                    .replaceAll(":month", DateUtils.getThisMonth())
                    .replaceAll(":year", DateUtils.getThisYear());
            sheetBbdsEvn.getRow(33).getCell(0).setCellValue(a5);
            sheetBbdsEvn.getRow(18).getCell(4).setCellValue(numberAuto + numberEpoint);
            sheetBbdsEvn.getRow(18).getCell(6).setCellValue(numberAuto + numberEpoint);
            sheetBbdsEvn.getRow(19).getCell(4).setCellValue(amountAuto + amountEpoint);
            sheetBbdsEvn.getRow(19).getCell(6).setCellValue(amountAuto + amountEpoint);

            // sheet DE NGHI THANH TOAN
            Sheet sheetDeNghiThanhToan = workbook.getSheetAt(2);
            String a6 = sheetDeNghiThanhToan.getRow(4).getCell(6).getStringCellValue()
                    .replaceAll(":day", DateUtils.getThisDay())
                    .replaceAll(":month", DateUtils.getThisMonth())
                    .replaceAll(":year", DateUtils.getThisYear());
            sheetDeNghiThanhToan.getRow(4).getCell(6).setCellValue(a6);
            String a7 = sheetDeNghiThanhToan.getRow(11).getCell(0).getStringCellValue()
                    .replaceAll(":fromDate", fromDate)
                    .replaceAll(":toDate", toDate);
            sheetDeNghiThanhToan.getRow(11).getCell(0).setCellValue(a7);
            String a8 = sheetDeNghiThanhToan.getRow(13).getCell(0).getStringCellValue()
                    .replaceAll(":fromDate", fromDate)
                    .replaceAll(":toDate", toDate);
            sheetDeNghiThanhToan.getRow(13).getCell(0).setCellValue(a8);
            String a9 = sheetDeNghiThanhToan.getRow(14).getCell(0).getStringCellValue()
                    .replaceAll(":fromDate", fromDate)
                    .replaceAll(":toDate", toDate);
            sheetDeNghiThanhToan.getRow(14).getCell(0).setCellValue(a9);
            if (totalAmount > 0) {
                String a10 = sheetDeNghiThanhToan.getRow(16).getCell(7).getStringCellValue()
                        .replaceAll(":soTien", Utils.numberToString(new BigDecimal(totalAmount + totalFee)));
                sheetDeNghiThanhToan.getRow(16).getCell(7).setCellValue(a10);
            }
            sheetDeNghiThanhToan.getRow(13).getCell(3).setCellValue(numberAuto);
            sheetDeNghiThanhToan.getRow(13).getCell(5).setCellValue(amountAuto);
            sheetDeNghiThanhToan.getRow(13).getCell(9).setCellValue(
                    Constant.EVNHN_FEE_INCLUDEDVAT_AUTO * numberAuto);
            sheetDeNghiThanhToan.getRow(14).getCell(3).setCellValue(numberEpoint);
            sheetDeNghiThanhToan.getRow(14).getCell(5).setCellValue(amountEpoint);
            sheetDeNghiThanhToan.getRow(14).getCell(9).setCellValue(
                    Constant.EVNHN_FEE_INCLUDEDVAT_EPOINT * numberEpoint);
            sheetDeNghiThanhToan.getRow(15).getCell(7).setCellValue(
                    amountAuto + (Constant.EVNHN_FEE_INCLUDEDVAT_AUTO * numberAuto)
                            + amountEpoint + (Constant.EVNHN_FEE_INCLUDEDVAT_EPOINT * numberEpoint));

            // sheet BANG KE
            Sheet sheetBangKe = workbook.getSheetAt(3);
            String a11 = sheetBangKe.getRow(3).getCell(0).getStringCellValue()
                    .replaceAll(":fromDate", fromDate)
                    .replaceAll(":toDate", toDate);
            sheetBangKe.getRow(3).getCell(0).setCellValue(a11);
            int i = 0;
            int rowIndexBangKe = 5;
            if (dataRows != null) {
                for (String[] line : dataRows) {
                    BangKe bangKe = new BangKe(line[0]
                            , line[1]
                            , line[2]
                            , line[3]
                            , line[4]
                            , line[5]
                            , Double.parseDouble(line[6])
                            , line[7]
                            , line[8]);

                    if (sheetBangKe.getRow(rowIndexBangKe) == null)
                        sheetBangKe.createRow(rowIndexBangKe);
                    // create row
                    Row row = sheetBangKe.getRow(rowIndexBangKe);
                    // create cell
                    // stt
                    Cell cell = row.getCell(0);
                    cell.setCellValue(rowIndexBangKe - 4);
                    // data
                    row.getCell(1).setCellValue(bangKe.getPayerId());
                    row.getCell(2).setCellValue(bangKe.getPayerName());
                    row.getCell(3).setCellValue(bangKe.getInvoiceId());
                    row.getCell(4).setCellValue(bangKe.getInvoiceType());
                    row.getCell(5).setCellValue(bangKe.getRoute());
                    row.getCell(6).setCellValue(bangKe.getTransId());
                    row.getCell(7).setCellValue(bangKe.getAmount());
                    row.getCell(8).setCellValue(bangKe.getDate());
                    row.getCell(9).setCellValue(bangKe.getService());
                    rowIndexBangKe++;
                    i++;
                }
            }

            if (rowIndexBangKe == 5) {
                rowIndexBangKe = 15;
            }

            if (sheetBangKe.getRow(rowIndexBangKe + 4) == null)
                sheetBangKe.createRow(rowIndexBangKe + 4);
            if (sheetBangKe.getRow(rowIndexBangKe + 4).getCell(1) == null)
                sheetBangKe.getRow(rowIndexBangKe + 4).createCell(1);
            if (sheetBangKe.getRow(rowIndexBangKe + 4).getCell(2) == null)
                sheetBangKe.getRow(rowIndexBangKe + 4).createCell(2);
            if (sheetBangKe.getRow(rowIndexBangKe + 5) == null)
                sheetBangKe.createRow(rowIndexBangKe + 5);
            if (sheetBangKe.getRow(rowIndexBangKe + 5).getCell(1) == null)
                sheetBangKe.getRow(rowIndexBangKe + 5).createCell(1);
            if (sheetBangKe.getRow(rowIndexBangKe + 5).getCell(2) == null)
                sheetBangKe.getRow(rowIndexBangKe + 5).createCell(2);
            sheetBangKe.getRow(rowIndexBangKe + 4).getCell(1).setCellValue("Tổng số hóa đơn: ");
            sheetBangKe.getRow(rowIndexBangKe + 4).getCell(2).setCellValue(i);
            sheetBangKe.getRow(rowIndexBangKe + 5).getCell(1).setCellValue("Tổng số tiền thanh toán: ");
            sheetBangKe.getRow(rowIndexBangKe + 5).getCell(2).setCellValue(totalAmount);
            String a13 = sheetBangKe.getRow(2).getCell(0).getStringCellValue()
                    .replaceAll(":Qh", fileName.toUpperCase());
            sheetBangKe.getRow(2).getCell(0).setCellValue(a13);

            file.close();
            FileOutputStream outputStream = new FileOutputStream(reconcileFile);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
            Thread.sleep(1000);

        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    private void createBean() {
        // neu null thi khoi tao moi
        if (transactionInAuthServices == null)
            transactionInAuthServices = BeanUtils.getBean(TransactionInauthServices.class);
        if (emailServiceOld == null)
            emailServiceOld = BeanUtils.getBean(EmailServiceOld.class);
        if (bankTransactionService == null)
            bankTransactionService = BeanUtils.getBean(BankTransactionServices.class);
        if (mapper == null)
            mapper = BeanUtils.getBean(ObjectMapper.class);
        if (evnReconcileEmailServices == null)
            evnReconcileEmailServices = BeanUtils.getBean(EvnReconcileEmailServices.class);
    }

}
