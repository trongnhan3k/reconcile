package vn.paytech.reconcile.quartz;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import vn.paytech.reconcile.config.Constant;
import vn.paytech.reconcile.config.LoadFileProperties;
import vn.paytech.reconcile.ewallet.entity.TransactionInauth;
import vn.paytech.reconcile.paygate.entity.BankTransaction;
import vn.paytech.reconcile.paygate.entity.EvnReconcileEmail;
import vn.paytech.reconcile.services.*;
import vn.paytech.reconcile.utils.BeanUtils;
import vn.paytech.reconcile.utils.EvnUtils;
import vn.paytech.reconcile.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@DisallowConcurrentExecution
public class EVNHaNoiReconcileJob implements Job {
    private static final Logger log = Logger.getLogger(EVNHaNoiReconcileJob.class);

    @Autowired
    private TransactionInauthServices transactionInAuthServices;

    @Autowired
    private BankTransactionServices bankTransactionService;

    @Autowired
    private EmailServiceOld emailServiceOld;

    @Autowired
    private EvnReconcileEmailServices evnReconcileEmailServices;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ObjectMapper mapper;

    private static String MERCHANTCODE = "EPOINT";

    private static String prefix = "auto_payment_";

    private static String AUTO = "AUTO";

    private static int totalTransaction = 0;

    private static Double totalAmount = 0.0;

    private static List<String> dayNotSendMail = new ArrayList<>();

    private static String fromDate = "";

    private static String toDate = "";

    private static DateFormat dateFormatEmail = new SimpleDateFormat("dd/MM/yyyy");

    private static DateFormat dateFormatFile = new SimpleDateFormat("yyyyMMdd");

    private static String TEMPLATE = "";

    public EVNHaNoiReconcileJob() {

    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
//        	List<String> lst = new ArrayList<String>();
//        	lst.add("D:\\Paytech\\RECONCILIATION\\reconcile\\report\\EPOINT\\20201225\\Phúc Thọ.xlsx");
//        	readExcelFile(lst);
            // TODO Auto-generated method stub
            log.info("EVN reconcile start.....");

            TEMPLATE = String.format("%s/%s/templates/daily-template.xlsx", LoadFileProperties.ReportDir,
                    MERCHANTCODE);

            // process fromDate, toDate
            dayNotSendMail.add("SAT");
            dayNotSendMail.add("SUN");

            Calendar cl = Calendar.getInstance();
            String day = cl.getTime().toString().split(" ")[0];

            if (day.equalsIgnoreCase("Mon")) {

                cl.add(Calendar.DAY_OF_MONTH, -1);
                Date issueToDate = cl.getTime();
                toDate = dateFormatEmail.format(issueToDate);

                cl.add(Calendar.DAY_OF_MONTH, -2);
                Date issueFromDate = cl.getTime();
                fromDate = dateFormatEmail.format(issueFromDate);

            } else if (!dayNotSendMail.contains(day.toUpperCase())) {
                cl.add(Calendar.DAY_OF_MONTH, -1);
                fromDate = dateFormatEmail.format(cl.getTime());
                toDate = dateFormatEmail.format(cl.getTime());
            }

            this.createBean();

            Map<String, List<String[]>> transactions = this.getData();

            Iterator iterator = Constant.mapQhEvn.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry pair = (Map.Entry) iterator.next();
                log.info((String) pair.getValue());
                this.createExcelFile((String) pair.getValue(), transactions.get(pair.getValue()));
                // iterator.remove(); // avoids a ConcurrentModificationException
            }
            //

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    private Map<String, List<String[]>> getData() throws JsonMappingException, JsonProcessingException {
        Map<String, List<String[]>> results = new LinkedHashMap<>();
        String[] dataRow = new String[]{};
        List<TransactionInauth> transactionInauths = transactionInAuthServices
                .lstPaymentTransCurrentDateByMerchant(MERCHANTCODE);
        log.info(transactionInauths.size());
        EvnUtils evnUtils = new EvnUtils();
        for (TransactionInauth transactionInauth : transactionInauths) {
            Map description = mapper.readValue(transactionInauth.getDescription(), Map.class);
            String userId = (String) description.get("userId");
            if (userId == null || userId.isEmpty()) {
                userId = (String) description.get("user_id");
            }
            String customerName = (String) description.get("userName");
            if (customerName == null || customerName.isEmpty()) {
                customerName = (String) description.get("user_name");
            }
            String evnQh = evnUtils.getEvnQh(userId);
            String channel = MERCHANTCODE;
            if (transactionInauth.getRefNo().startsWith(prefix)) {
                channel = AUTO;
            }
            dataRow = new String[]{userId, customerName, transactionInauth.getReceiverInfo(), "", "",
                    transactionInauth.getTransactionNo(), String.valueOf(transactionInauth.getAmount()),
                    String.valueOf(transactionInauth.getCreateDate()), channel};

            if (evnQh == null) {
                log.info(mapper.writeValueAsString(dataRow));
                continue;
            }

            List<String[]> data = new ArrayList<>();

            if (results.containsKey(evnQh)) {
                data = results.get(evnQh);
            }
            data.add(dataRow);
            results.put(evnQh, data);

        }

        List<BankTransaction> bankTransactions = bankTransactionService.findAllByMerchantNameAndDate(MERCHANTCODE);
        log.info(bankTransactions.size());
        for (BankTransaction bankTransaction : bankTransactions) {
            Map moreInfo = mapper.readValue(bankTransaction.getMoreInfo(), Map.class);

            String userId = (String) moreInfo.get("userId");
            String evnQh = evnUtils.getEvnQh(userId);

            dataRow = new String[]{(String) moreInfo.get("userId"), (String) moreInfo.get("userName"),
                    bankTransaction.getCustomerId(), "", "", bankTransaction.getTransId(),
                    String.valueOf(bankTransaction.getNumAmount()), bankTransaction.getIpnResptime().toString(),
                    MERCHANTCODE};

            if (evnQh == null) {
                log.info(mapper.writeValueAsString(dataRow));
                continue;
            }

            List<String[]> data = new ArrayList<>();

            if (results.containsKey(evnQh)) {
                data = results.get(evnQh);
            }
            data.add(dataRow);
            results.put(evnQh, data);
        }
        return results;
    }

    private void createExcelFile(String fileName, List<String[]> dataRows) throws IOException {
        Date date = Calendar.getInstance().getTime();

        String pathFile = String.format("%s%s/%s/", LoadFileProperties.ReportDir, MERCHANTCODE,
                dateFormatFile.format(date));

        File dir = new File(pathFile);
        if (!dir.exists())
            dir.mkdirs();

        String reconcileFile = pathFile + fileName + ".xlsx";

        File f = new File(reconcileFile);
        if (f.exists() && !f.isDirectory()) {
            f.delete();
        }


        FileUtils.copyFile(new File(TEMPLATE), f);

        this.writeToExcelFile(fileName, reconcileFile, dataRows);

        return;
    }

    private void writeToExcelFile(String fileName, String reconcileFile, List<String[]> dataRows) throws IOException {
        try {
            File jfile = new File(reconcileFile);
            FileInputStream file = new FileInputStream(jfile);
            Workbook workbook = new XSSFWorkbook(file);

            Sheet sheet = workbook.getSheetAt(0);

            // body
            int rowIndex = 2;
            if (dataRows != null) {
                for (String[] line : dataRows) {
                    // create row
                    Row row = sheet.getRow(rowIndex);
                    // create cell
                    // stt
                    Cell cell = row.getCell(0);
                    cell.setCellValue(rowIndex - 1);
                    // data
                    for (int celIndex = 0; celIndex < line.length; celIndex++) {
                        cell = row.getCell(celIndex + 1);
                        cell.setCellValue(line[celIndex]);
                    }
                    rowIndex++;
                }
                file.close();
                FileOutputStream outputStream = new FileOutputStream(reconcileFile);
                workbook.write(outputStream);
                workbook.close();
                outputStream.close();
            }

            List<EvnReconcileEmail> listEmail = evnReconcileEmailServices.listEmailByQhName(fileName);
            List<String> listTo = new ArrayList<>();
            List<String> listCc = new ArrayList<>();
            if (listEmail.size() > 0) {
                for (EvnReconcileEmail evnReconcileEmail : listEmail) {
                    String email = evnReconcileEmail.getEmail();
                    if (evnReconcileEmail.getIsCc().equals(0)) {
                        listTo.add(email);
                    } else {
                        listCc.add(email);
                    }
                }
            }
            String[] to = listTo.toArray(new String[0]);
            String[] cc = listCc.toArray(new String[0]);
            Calendar c = Calendar.getInstance();
            String day = c.getTime().toString().split(" ")[0];

            List<String> listFile = new ArrayList<>();

            if (day.equalsIgnoreCase("Mon")) {
                for (int i = 0; i <= 2; i++) {
                    Calendar cl = Calendar.getInstance();
                    cl.add(Calendar.DAY_OF_MONTH, -i);
                    Date date = cl.getTime();
                    listFile.add(String.format("%s%s/%s/%s", LoadFileProperties.ReportDir, MERCHANTCODE,
                            dateFormatFile.format(date), jfile.getName()));
                }
                sendEmail(to, cc, listFile, fromDate, toDate);
            } else if (!dayNotSendMail.contains(day.toUpperCase())) {
                listFile.add(reconcileFile);
                sendEmail(to, cc, listFile, fromDate, toDate);
            }

            Thread.sleep(1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Double> readExcelFile(List<String> filePaths) throws IOException {
        List<Double> data = new ArrayList<>();
        for (String filePath : filePaths) {
            FileInputStream file = new FileInputStream(new File(filePath));
            Workbook workbook = new XSSFWorkbook(file);
            Sheet sheet = workbook.getSheetAt(0);
            int i = 0;
            for (Row row : sheet) {
                if (i > 1) {
                    Cell cell = row.getCell(7);
                    if (cell.getStringCellValue().equals("")
                            || cell.getStringCellValue() == null)
                        break;
                    data.add(Double.parseDouble(cell.getStringCellValue()));
                }
                i++;
            }
        }
        return data;
    }

    private void sendEmail(String[] to, String[] cc, List<String> attachPath, String fromDate, String toDate)
            throws IOException {
        Date date = new Date();

        String subject = String.format(Constant.EVN_RECONCILE_SUBJECT, dateFormatEmail.format(date), fromDate, toDate);

        List<Double> txns = readExcelFile(attachPath);
        Double total = txns.stream().mapToDouble(Double::doubleValue).sum();
        // fromdate, todate, so gd, gia tri gd, tien thanh toan
        String body = String.format(Constant.EVN_RECONCILE_TEXT, fromDate, toDate, txns.size(), Utils.withLargeIntegers(total), Utils.withLargeIntegers(total));
        
        emailService.sendEmail(attachPath.toArray(new String[attachPath.size()]), body, subject, to, cc, null);
//        emailService.sendMessageWithAttachment(to, cc, subject, body, attachPath);
    }

    private void createBean() {
        // neu null thi khoi tao moi
        if (transactionInAuthServices == null)
            transactionInAuthServices = BeanUtils.getBean(TransactionInauthServices.class);
        if (emailServiceOld == null)
            emailServiceOld = BeanUtils.getBean(EmailServiceOld.class);
        if (bankTransactionService == null)
            bankTransactionService = BeanUtils.getBean(BankTransactionServices.class);
        if (mapper == null)
            mapper = BeanUtils.getBean(ObjectMapper.class);
        if (evnReconcileEmailServices == null)
            evnReconcileEmailServices = BeanUtils.getBean(EvnReconcileEmailServices.class);
        if (emailService == null)
            emailService = BeanUtils.getBean(EmailService.class);
    }

}
