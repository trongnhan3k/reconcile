package vn.paytech.reconcile.ewallet;



import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "vn.paytech.reconcile.ewallet", entityManagerFactoryRef = "ewalletEntityManager", transactionManagerRef = "ewalletTransactionManager")
public class EwalletConfiguration {
	
	@Autowired
    private Environment env;

	public EwalletConfiguration() {
		super();
	}

	@Primary
	@Bean(name = "dataSource")
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource ewalletDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	    dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
	    dataSource.setUrl(env.getProperty("spring.datasource.url"));
	    dataSource.setUsername(env.getProperty("spring.datasource.username"));
	    dataSource.setPassword(env.getProperty("spring.datasource.password"));

	    return dataSource;
		//return DataSourceBuilder.create().build();
	}

	@Primary
	@Bean(name = "ewalletEntityManager")
	public LocalContainerEntityManagerFactoryBean ewalletEntityManager(EntityManagerFactoryBuilder builder,
			@Qualifier("dataSource") DataSource dataSource) {
		System.out.println("ewallet loading config");
		return builder.dataSource(dataSource).packages("vn.paytech.reconcile.ewallet.entity").persistenceUnit("ewallet")
				.build();
	}

	@Primary
	@Bean(name = "ewalletTransactionManager")
	public PlatformTransactionManager ewalletTransactionManager(
			@Qualifier("ewalletEntityManager") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}


}
