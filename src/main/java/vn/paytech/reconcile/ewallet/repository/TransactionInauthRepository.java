package vn.paytech.reconcile.ewallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.paytech.reconcile.ewallet.entity.TransactionInauth;

import java.util.List;

@Repository
public interface TransactionInauthRepository extends JpaRepository<TransactionInauth, String> {

	@Query(value = "select * from TRANSACTION_INAUTH s where DAT_CREATEDATE >= trunc(sysdate) "
			+ "and DAT_CREATEDATE <= trunc(sysdate) + 1 and INT_STATUS = 2", nativeQuery = true)
	List<TransactionInauth> lstTransCurrentDate();
	
	@Query(value = "select * from TRANSACTION_INAUTH s where INT_STATUS = 2 and STR_REFCODE = 'VASTRANSFER'", nativeQuery = true)
	List<TransactionInauth> lstTransVAS();

	@Query("select s from TransactionInauth s where s.transactionNo = ?1")
	TransactionInauth transByTransId(String transId);

	@Query(value = "select * from TRANSACTION_INAUTH s "
			+ "where DAT_CREATEDATE > trunc(sysdate) -1 and DAT_CREATEDATE < trunc(sysdate) and "
			+ "INT_STATUS = 2 and STR_REFCODE = 'VASTRANSFER' and STR_CHANNEL = :merchantCode", nativeQuery = true)
	List<TransactionInauth> lstPaymentTransCurrentDateByMerchant(String merchantCode);
	
	@Query(value = "select * from TRANSACTION_INAUTH s "
			+ "where "
			+ "INT_STATUS = 2 and STR_REFCODE = 'VASTRANSFER' and STR_CHANNEL = :merchantCode "
			+ " AND s.DAT_CREATEDATE >= TO_DATE(:fromDate, 'dd-MM-yyyy') AND s.DAT_CREATEDATE <= TO_DATE(:toDate, 'dd-MM-yyyy') ", nativeQuery = true)
	List<TransactionInauth> lstPaymentTransByMerchant(String merchantCode, String fromDate, String toDate);

	@Query("select s from TransactionInauth s where s.refNo = ?1")
	TransactionInauth transByRefNo(String refNo);

	@Query(value = "select 'EWALLET' PAYMENT_METHOD, TO_CHAR(s.dat_createdate, 'yyyy-MM-dd') create_Date, SUM(NUM_AMOUNT) TOTAL_AMOUNT"
			+ ", COUNT(*) TOTAL_TRANS, s.int_status, s.str_channel " + ", s.str_refcode, s.STR_BANKCODE "
			+ "from Transaction_Inauth s "
			+ "where s.DAT_CREATEDATE >= trunc(sysdate) - 1 and s.DAT_CREATEDATE < trunc(sysdate) "
			+ "GROUP BY s.int_status, s.str_channel, s.str_refcode, s.STR_BANKCODE, TO_CHAR(s.dat_createdate, 'yyyy-MM-dd') ", nativeQuery = true)
	List<Object[]> lstTransLastDay();

	@Query(value = "select 'EWALLET' PAYMENT_METHOD, TO_CHAR(s.dat_createdate, 'yyyy-MM-dd') create_Date, SUM(NUM_AMOUNT) TOTAL_AMOUNT"
			+ ", COUNT(*) TOTAL_TRANS, s.int_status, s.str_channel " + ", s.str_refcode, s.STR_BANKCODE "
			+ "from Transaction_Inauth s " + "where s.DAT_CREATEDATE >= TO_DATE(:fromDate, 'yyyy-MM-dd') "
			+ "AND s.DAT_CREATEDATE < TO_DATE(:toDate, 'yyyy-MM-dd') + 1 "
			+ "GROUP BY s.int_status, s.str_channel, s.str_refcode, s.STR_BANKCODE, TO_CHAR(s.dat_createdate, 'yyyy-MM-dd')", nativeQuery = true)
	List<Object[]> lstTransLastDay(String fromDate, String toDate);
}
