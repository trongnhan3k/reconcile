package vn.paytech.reconcile.ewallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.paytech.reconcile.ewallet.entity.Customers;

import java.util.List;

@Repository
public interface BankAccountsRepository extends JpaRepository<Customers, String> {

    @Query(value = "SELECT str_bankcode, COUNT(str_username) " +
            "FROM bankaccounts " +
            "WHERE int_status = 2 " +
            "AND int_islinkbank = 1 " +
            "GROUP BY str_bankcode", nativeQuery = true)
    List<Object[]> totalLinkedBankUsers();
}
