package vn.paytech.reconcile.ewallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.paytech.reconcile.ewallet.entity.Customers;

@Repository
public interface AccountsRepository extends JpaRepository<Customers, String> {

    @Query(value = "SELECT SUM(num_balance) " +
            "FROM accounts " +
            "WHERE int_accounttype = 0 ", nativeQuery = true)
    Double totalBalanceAmount();
}
