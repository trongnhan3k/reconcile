package vn.paytech.reconcile.ewallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.paytech.reconcile.ewallet.entity.Customers;
import vn.paytech.reconcile.ewallet.entity.TransactionInauth;

import java.util.List;

@Repository
public interface CustomersRepository extends JpaRepository<Customers, String> {

    @Query(value = "SELECT COUNT(*) new_users, s.int_status, TO_CHAR(s.dat_createdate, 'yyyy-MM-dd') " +
            "FROM customers s " +
            "WHERE s.dat_createdate >= TO_DATE(:fromDate,'yyyy-MM-dd') " +
            "AND s.dat_createdate < TO_DATE(:toDate,'yyyy-MM-dd') + 1 " +
            "AND s.int_status != 0 " +
            "GROUP BY s.int_status, TO_CHAR(s.dat_createdate, 'yyyy-MM-dd')", nativeQuery = true)
    List<Object[]> lstNewUsersByDay(String fromDate, String toDate);

    @Query(value = "SELECT COUNT(*) new_users, s.int_status, TO_CHAR(s.dat_createdate, 'yyyy-MM-dd') " +
            "FROM customers s " +
            "WHERE s.dat_createdate >= trunc(sysdate) - 1 and s.dat_createdate < trunc(sysdate) " +
            "AND s.int_status != 0 " +
            "GROUP BY s.int_status, TO_CHAR(s.dat_createdate, 'yyyy-MM-dd')", nativeQuery = true)
    List<Object[]> lstNewUsersLastDay();
}
