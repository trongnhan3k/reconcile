package vn.paytech.reconcile.ewallet.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNTS")
@Getter
@Setter
public class Accounts {

    @Id
    @Column(name = "STR_ACCOUNTNUMBER")
    private String accountNumber;

    @Column(name = "INT_ACCOUNTTYPE")
    private int accountType;

    @Column(name = "NUM_BALANCE")
    private Double numBalance;

}
