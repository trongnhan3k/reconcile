package vn.paytech.reconcile.ewallet.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name="TRANSACTION_INAUTH")
@Getter
@Setter
public class TransactionInauth {

   
    @Column(name = "STR_USERNAME")
    private String username;

    @Column(name = "NUM_AMOUNT")
    private int amount;
    
    @Id
    @Column(name = "STR_TRANSACTIONNO")
    private String transactionNo;

    @Column(name = "INT_TRANSACTIONTYPE")
    private int transactionType;

    @Column(name = "STR_REFNO")
    private String refNo;

    @Column(name = "STR_DEBITACCOUNT")
    private String debitAccount;

    @Column(name = "STR_CREDITACCOUNT")
    private String creditAccount;

    @Column(name = "INT_CURRENCYCODE")
    private int currencyCode;

    @Column(name = "STR_SENDERINFO")
    private String senderInfo;

    @Column(name = "STR_RECEIVERINFO")
    private String receiverInfo;

    @Column(name = "STR_REFCODE")
    private String refCode;

    @Column(name = "STR_DESCRIPTION")
    private String description;

    @Column(name = "NUM_FEEAMOUNT")
    private int feeAmount;

    @Column(name = "NUM_VATAMOUNT")
    private int vatAmount;

    @Column(name = "STR_FEEACC")
    private String feeAcc;

    @Column(name = "STR_VATACC")
    private String vatAcc;

    @Column(name = "STR_ISFEEFROMCUS")
    private String isFeeFromCus;

    @Column(name = "INT_STATUS")
    private int status;

    @Column(name = "DAT_CREATEDATE")
    private Date createDate;

    @Column(name = "STR_CREATEUSER")
    private String createUser;

    @Column(name = "DAT_AUTHDATE")
    private Date authDate;

    @Column(name = "STR_AUTHUSER")
    private String authUser;

    @Column(name = "STR_CHANNEL")
    private String channel;

    @Column(name = "STR_ISDEBIT")
    private String isDebit;

    @Column(name = "STR_DEPENDACCOUNT")
    private String dependaAccount;
    
    @Column(name = "STR_BANKCODE")
    private String bankCode;
    
    @Column(name = "INT_ISLINKBANK")
    private int isLinkBank;
    

    @Override
    public String toString() {
        return "Transaction{" +
                "username='" + this.username + '\'' +
                ", amount=" + this.amount +
                ", transactionNo='" + this.transactionNo + '\'' +
                ", transactionType=" + this.transactionType +
                ", refNo='" + refNo + '\'' +
                ", debitAccount='" + debitAccount + '\'' +
                ", creditAccount='" + creditAccount + '\'' +
                ", currencyCode=" + currencyCode +
                ", senderInfo='" + senderInfo + '\'' +
                ", receiverInfo='" + receiverInfo + '\'' +
                ", refCode='" + refCode + '\'' +
                ", description='" + description + '\'' +
                ", feeAmount=" + feeAmount +
                ", vatAmount=" + vatAmount +
                ", feeAcc='" + feeAcc + '\'' +
                ", vatAcc='" + vatAcc + '\'' +
                ", isFeeFromCus='" + isFeeFromCus + '\'' +
                ", status=" + status +
                ", createDate=" + createDate +
                ", createUser='" + createUser + '\'' +
                ", authDate=" + authDate +
                ", aythUser='" + authUser + '\'' +
                ", channel='" + channel + '\'' +
                ", isDebit='" + isDebit + '\'' +
                ", dependaAccount=" + dependaAccount +
                '}';
    }



	public TransactionInauth() {
		super();
		// TODO Auto-generated constructor stub
	}
}
