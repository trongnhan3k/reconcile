package vn.paytech.reconcile.ewallet.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CUSTOMERS")
@Getter
@Setter
public class Customers {

    @Id
    @Column(name = "STR_USERNAME")
    private String username;

    @Column(name = "INT_STATUS")
    private int status;

    @Column(name = "DAT_CREATEDATE")
    private Date createDate;

}
