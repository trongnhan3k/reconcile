package vn.paytech.reconcile.ewallet.model;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionInauthReport {
    @SerializedName("CREATE_DATE")
    private String createDate;
    @SerializedName("TOTAL_AMOUNT")
    private String totalAmount;
    @SerializedName("INT_STATUS")
    private String status;
    @SerializedName("STR_CHANNEL")
    private String channel;
    @SerializedName("STR_REFCODE")
    private String refCode;
    @SerializedName("STR_BANKCODE")
    private String bankCode;
}
