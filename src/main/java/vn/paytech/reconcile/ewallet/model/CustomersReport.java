package vn.paytech.reconcile.ewallet.model;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomersReport {

    private String createDate;
    private Integer newUsers;
    private Integer status;
}
