package vn.paytech.reconcile.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BBXN_DT_Phi {
    private Integer numberEpoint;
    private Integer numberAuto;
    private Double amountEpoint;
    private Double amountAuto;
    private Double feeEpoint;
    private Double feeAuto;

    public BBXN_DT_Phi() {
    }

    public BBXN_DT_Phi(Integer numberEpoint, Integer numberAuto, Double amountEpoint, Double amountAuto, Double feeEpoint, Double feeAuto) {
        this.numberEpoint = numberEpoint;
        this.numberAuto = numberAuto;
        this.amountEpoint = amountEpoint;
        this.amountAuto = amountAuto;
        this.feeEpoint = feeEpoint;
        this.feeAuto = feeAuto;
    }

    @Override
    public String toString() {
        return "BBXN_DT_Phi{" +
                "numberEpoint=" + numberEpoint +
                ", numberAuto=" + numberAuto +
                ", amountEpoint=" + amountEpoint +
                ", amountAuto=" + amountAuto +
                ", feeEpoint=" + feeEpoint +
                ", feeAuto=" + feeAuto +
                '}';
    }
}
