package vn.paytech.reconcile.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class LinkBankModel {
    private String bankCode;
    private Integer users;

    public LinkBankModel() {
    }

    public LinkBankModel(String bankCode, Integer users) {
        this.bankCode = bankCode;
        this.users = users;
    }
}
