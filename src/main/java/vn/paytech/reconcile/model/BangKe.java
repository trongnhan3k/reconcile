package vn.paytech.reconcile.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BangKe {
    private String payerId;
    private String payerName;
    private String invoiceId;
    private String invoiceType;
    private String route;
    private String transId;
    private Double amount;
    private String date;
    private String service;

    public BangKe() {
    }

    public BangKe(String payerId, String payerName, String invoiceId, String invoiceType, String route, String transId, Double amount, String date, String service) {
        this.payerId = payerId;
        this.payerName = payerName;
        this.invoiceId = invoiceId;
        this.invoiceType = invoiceType;
        this.route = route;
        this.transId = transId;
        this.amount = amount;
        this.date = date;
        this.service = service;
    }
}
