package vn.paytech.reconcile.napas.model;

public class NapasAcqModel {
	//[]                                                                                                    []                                                                                                    []f3966d4e81fb878b947991db9d084eee
	
	private String MTI;
	private String F2;
	private String F3;
	private String SVC;
	private String TCC;
	private String F4;
	private String RTA;
	private String F49;
	private String F5;
	private String F50;
	private String F9;
	private String F6;
	private String RCA;
	private String F51;
	private String F10;
	private String F11;
	private String F12;
	private String F13;
	private String F15;
	private String F18;
	private String F22;
	private String F25;
	private String F41;
	private String ACQ;
	private String ISS;
	private String MID;
	private String BNB;
	private String F102;
	private String F103;
	private String SVFISSNP;
	private String IRFISSACQ;
	private String IRFISSBNB;
	private String SVFACQNP;
	private String IRFACQISS;
	private String IRFACQBNB;
	private String SVFBNBNP;
	private String IRFBNBISS;
	private String IRFBNBACQ;
	private String F37;
	private String F38;
	private String TRN;
	private String RRC;
	private String RSV1;
	private String RSV2;
	private String RSV3;
	private String CSR;
	
	
	
	public String getMTI() {
		return MTI;
	}



	public void setMTI(String mTI) {
		MTI = mTI;
	}



	public String getF2() {
		return F2;
	}



	public void setF2(String f2) {
		F2 = f2;
	}



	public String getF3() {
		return F3;
	}



	public void setF3(String f3) {
		F3 = f3;
	}



	public String getSVC() {
		return SVC;
	}



	public void setSVC(String sVC) {
		SVC = sVC;
	}



	public String getTCC() {
		return TCC;
	}



	public void setTCC(String tCC) {
		TCC = tCC;
	}



	public String getF4() {
		return F4;
	}



	public void setF4(String f4) {
		F4 = f4;
	}



	public String getRTA() {
		return RTA;
	}



	public void setRTA(String rTA) {
		RTA = rTA;
	}



	public String getF49() {
		return F49;
	}



	public void setF49(String f49) {
		F49 = f49;
	}



	public String getF5() {
		return F5;
	}



	public void setF5(String f5) {
		F5 = f5;
	}



	public String getF50() {
		return F50;
	}



	public void setF50(String f50) {
		F50 = f50;
	}



	public String getF9() {
		return F9;
	}



	public void setF9(String f9) {
		F9 = f9;
	}



	public String getF6() {
		return F6;
	}



	public void setF6(String f6) {
		F6 = f6;
	}



	public String getRCA() {
		return RCA;
	}



	public void setRCA(String rCA) {
		RCA = rCA;
	}



	public String getF51() {
		return F51;
	}



	public void setF51(String f51) {
		F51 = f51;
	}



	public String getF10() {
		return F10;
	}



	public void setF10(String f10) {
		F10 = f10;
	}



	public String getF11() {
		return F11;
	}



	public void setF11(String f11) {
		F11 = f11;
	}



	public String getF12() {
		return F12;
	}



	public void setF12(String f12) {
		F12 = f12;
	}



	public String getF13() {
		return F13;
	}



	public void setF13(String f13) {
		F13 = f13;
	}



	public String getF15() {
		return F15;
	}



	public void setF15(String f15) {
		F15 = f15;
	}



	public String getF18() {
		return F18;
	}



	public void setF18(String f18) {
		F18 = f18;
	}



	public String getF22() {
		return F22;
	}



	public void setF22(String f22) {
		F22 = f22;
	}



	public String getF25() {
		return F25;
	}



	public void setF25(String f25) {
		F25 = f25;
	}



	public String getF41() {
		return F41;
	}



	public void setF41(String f41) {
		F41 = f41;
	}



	public String getACQ() {
		return ACQ;
	}



	public void setACQ(String aCQ) {
		ACQ = aCQ;
	}



	public String getISS() {
		return ISS;
	}



	public void setISS(String iSS) {
		ISS = iSS;
	}



	public String getMID() {
		return MID;
	}



	public void setMID(String mID) {
		MID = mID;
	}



	public String getBNB() {
		return BNB;
	}



	public void setBNB(String bNB) {
		BNB = bNB;
	}



	public String getF102() {
		return F102;
	}



	public void setF102(String f102) {
		F102 = f102;
	}



	public String getF103() {
		return F103;
	}



	public void setF103(String f103) {
		F103 = f103;
	}



	public String getSVFISSNP() {
		return SVFISSNP;
	}



	public void setSVFISSNP(String sVFISSNP) {
		SVFISSNP = sVFISSNP;
	}



	public String getIRFISSACQ() {
		return IRFISSACQ;
	}



	public void setIRFISSACQ(String iRFISSACQ) {
		IRFISSACQ = iRFISSACQ;
	}



	public String getIRFISSBNB() {
		return IRFISSBNB;
	}



	public void setIRFISSBNB(String iRFISSBNB) {
		IRFISSBNB = iRFISSBNB;
	}



	public String getSVFACQNP() {
		return SVFACQNP;
	}



	public void setSVFACQNP(String sVFACQNP) {
		SVFACQNP = sVFACQNP;
	}



	public String getIRFACQISS() {
		return IRFACQISS;
	}



	public void setIRFACQISS(String iRFACQISS) {
		IRFACQISS = iRFACQISS;
	}



	public String getIRFACQBNB() {
		return IRFACQBNB;
	}



	public void setIRFACQBNB(String iRFACQBNB) {
		IRFACQBNB = iRFACQBNB;
	}



	public String getSVFBNBNP() {
		return SVFBNBNP;
	}



	public void setSVFBNBNP(String sVFBNBNP) {
		SVFBNBNP = sVFBNBNP;
	}



	public String getIRFBNBISS() {
		return IRFBNBISS;
	}



	public void setIRFBNBISS(String iRFBNBISS) {
		IRFBNBISS = iRFBNBISS;
	}



	public String getIRFBNBACQ() {
		return IRFBNBACQ;
	}



	public void setIRFBNBACQ(String iRFBNBACQ) {
		IRFBNBACQ = iRFBNBACQ;
	}



	public String getF37() {
		return F37;
	}



	public void setF37(String f37) {
		F37 = f37;
	}



	public String getF38() {
		return F38;
	}



	public void setF38(String f38) {
		F38 = f38;
	}



	public String getTRN() {
		return TRN;
	}



	public void setTRN(String tRN) {
		TRN = tRN;
	}



	public String getRRC() {
		return RRC;
	}



	public void setRRC(String rRC) {
		RRC = rRC;
	}



	public String getRSV1() {
		return RSV1;
	}



	public void setRSV1(String rSV1) {
		RSV1 = rSV1;
	}



	public String getRSV2() {
		return RSV2;
	}



	public void setRSV2(String rSV2) {
		RSV2 = rSV2;
	}



	public String getRSV3() {
		return RSV3;
	}



	public void setRSV3(String rSV3) {
		RSV3 = rSV3;
	}



	public String getCSR() {
		return CSR;
	}



	public void setCSR(String cSR) {
		CSR = cSR;
	}



	public NapasAcqModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
