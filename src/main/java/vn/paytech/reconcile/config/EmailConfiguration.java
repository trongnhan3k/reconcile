package vn.paytech.reconcile.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;


@Configuration
@ComponentScan(basePackages = { "vn.paytech.reconcile.services" })
public class EmailConfiguration {
    
	
    @Bean
    public JavaMailSender getJavaMailSender() {
    	System.out.println("getJavaMailSender=========================");
    	System.out.println(LoadFileProperties.Host == null ? "smtp.yandex.com" : LoadFileProperties.Host);
    	System.out.println(LoadFileProperties.EmailPort == null ? "25": LoadFileProperties.EmailPort);
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(LoadFileProperties.Host == null ? "smtp.yandex.com" : LoadFileProperties.Host);
        mailSender.setPort(Integer.parseInt(LoadFileProperties.EmailPort == null ? "25": LoadFileProperties.EmailPort));
        
        mailSender.setUsername(LoadFileProperties.EmailUsername == null ? "no-reply@vitapay.vn":LoadFileProperties.EmailUsername);
        mailSender.setPassword(LoadFileProperties.EmailPassword == null ? "123456a@":LoadFileProperties.EmailPassword);
        
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", LoadFileProperties.EmailAuth == null ? "true":LoadFileProperties.EmailAuth);
        props.put("mail.smtp.starttls.enable", LoadFileProperties.EmailStarttls == null ? "true":LoadFileProperties.EmailStarttls);
        props.put("mail.debug", "true");
        
        return mailSender;
    }
    
    @Bean
    public SimpleMailMessage templateSimpleMessage() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setText("This is the test email template for your email:\n%s\n");
        return message;
    }
    
    @Bean
    public SpringTemplateEngine thymeleafTemplateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(thymeleafTemplateResolver());
        templateEngine.setTemplateEngineMessageSource(emailMessageSource());
        return templateEngine;
    }

    @Bean
    public SpringResourceTemplateResolver thymeleafTemplateResolver() {
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setPrefix("/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");
        templateResolver.setCharacterEncoding("UTF-8");
        return templateResolver;
    }
    
//    @Bean
//    public FreeMarkerConfigurer freemarkerConfig() {
//        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
//        freeMarkerConfigurer.setTemplateLoaderPath("/");
//        return freeMarkerConfigurer;
//    }
//
//    @Bean
//    public FreeMarkerViewResolver freemarkerViewResolver() {
//        FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
//        resolver.setCache(true);
//        resolver.setPrefix("");
//        resolver.setSuffix(".ftl");
//        return resolver;
//    }

    
    @Bean
    public ResourceBundleMessageSource emailMessageSource() {
        final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("/mailMessages");
        return messageSource;
    }

}
