package vn.paytech.reconcile.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nhanvt on 2020/06/11
 */
public class Constant {

    public static Map<String, String> mapQhEvn;
    public static Map<String, String> mapIpnResponse;
    public static Double EVNHN_FEE_INCLUDEDVAT_AUTO = 1760.0;
    public static Double EVNHN_FEE_INCLUDEDVAT_EPOINT = 1540.0;
    public static String RECONCILE_SENDER = "doisoat@paytech.vn";
    public static String EMAIL_API_URL = "";

    public static String EVN_RECONCILE_SUBJECT = "[Paytech - EVNHN] Ngày %s (Số liệu từ ngày %s đến %s)";
    public static String EVN_RECONCILE_TEXT = "Kính gửi anh/chị,\n"
            + "Xin giới thiệu, em Phương ở Công ty CP PayTech, đơn vị đang hợp tác với Tổng Công ty điện lực Thành phố Hà Nội, theo Hợp đồng số 137/HĐ-EVN HANOI, ngày 3/10/2020.\n"
            + "Em xin gửi anh/chị thông tin khách hàng thanh toán hóa đơn tiền điện trên Kênh giao dịch của PayTech, từ ngày %s đến ngày %s , như sau:\n"
            + "- Tổng số giao dịch: %s GD\n"
            + "- Giá trị giao dịch: %s VNĐ\n"
            + "- Tổng số tiền thanh toán: %s VNĐ\n"
            + "Em xin gửi anh/chị kèm theo báo cáo tổng hợp giao dịch. Em rất mong nhận được phản hồi của anh/chị để sớm hoàn thành việc gạch nợ hóa đơn của Khách hàng ạ! .\n"
            + "Em Phương - 0914923008\n"
            + "Email - doisoat@paytech.vn\n"
            + "Em cảm ơn!\n";

    public static String PAYTECH_RECONCILE_SUBJECT = "Thống kê danh sách giao dịch ngày %s";
    public static String PAYTECH_RECONCILE_TEXT = "Kính gửi anh/chị,\n"
            + "Em xin gửi anh/chị thông tin giao dịch trên hệ thống thanh toán của PAYTECH, ngày %s, như sau:\n"
            + "- Tổng số giao dịch: %s GD.\n"
            + "- Giao dịch nạp: [Số lượng]: %s GD, [Giá trị]: %s VNĐ.\n"
            + "- Giao dịch rút: [Số lượng]: %s GD, [Giá trị]: %s VNĐ.\n"
            + "- Giao dịch chuyển tiền: [Số lượng]: %s GD, [Giá trị]: %s VNĐ.\n"
            + "- Giao dịch TOPUP: [Số lượng]: %s GD, [Giá trị]: %s VNĐ.\n"
            + "- Giao dịch thanh toán hóa đơn trên ví VitaPay: [Số lượng]: %s GD, [Giá trị]: %s VNĐ.\n"
            + "- Giao dịch thanh toán hóa đơn trên cổng thanh toán: [Số lượng]: %s GD, [Giá trị]: %s VNĐ.\n"
            + "- Tổng số tiền thanh toán: %s VNĐ.\n"
            + "- Vui lòng kiểm tra file đính kèm.\n"
            + "Em cảm ơn!\n";

    static {
        mapQhEvn = new HashMap<>();
        mapQhEvn.put("PD01", "Hoàn Kiếm");
        mapQhEvn.put("PD02", "Hai Bà Trưng");
        mapQhEvn.put("PD03", "Ba Đình");
        mapQhEvn.put("PD04", "Đống Đa");
        mapQhEvn.put("PD05", "Nam Từ Liêm");
        mapQhEvn.put("PD06", "Thanh Trì");
        mapQhEvn.put("PD07", "Gia Lâm");
        mapQhEvn.put("PD08", "Đông Anh");
        mapQhEvn.put("PD09", "Sóc Sơn");
        mapQhEvn.put("PD10", "Tây Hồ");
        mapQhEvn.put("PD11", "Thanh Xuân");
        mapQhEvn.put("PD12", "Cầu Giấy");
        mapQhEvn.put("PD13", "Hoàng Mai");
        mapQhEvn.put("PD14", "Long Biên");
        mapQhEvn.put("PD15", "Mê Linh");
        mapQhEvn.put("PD16", "Hà Đông");
        mapQhEvn.put("PD17", "Sơn Tây");
        mapQhEvn.put("PD18", "Chương Mỹ");
        mapQhEvn.put("PD19", "Thạch Thất");
        mapQhEvn.put("PD20", "Thường Tín");
        mapQhEvn.put("PD21", "Ba Vì");
        mapQhEvn.put("PD22", "Đan Phượng");
        mapQhEvn.put("PD23", "Hoài Đức");
        mapQhEvn.put("PD24", "Mỹ Đức");
        mapQhEvn.put("PD25", "Phú Xuyên");
        mapQhEvn.put("PD26", "Phúc Thọ");
        mapQhEvn.put("PD27", "Quốc Oai");
        mapQhEvn.put("PD28", "Thanh Oai");
        mapQhEvn.put("PD29", "Ứng Hòa");
        mapQhEvn.put("PD30", "Bắc Từ Liêm");
        mapQhEvn.put("", "Unkown");

        mapIpnResponse = new HashMap<>();
        mapIpnResponse.put("0", "SUCCESS");
        mapIpnResponse.put("000", "SUCCESS");
    }

}
