package vn.paytech.reconcile.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LoadFileProperties {
	public static String Napas_remoteHost;
    public static String Napas_port;
    public static String Napas_username;
    public static String Napas_password;
    public static String Napas_knownHostsFileLoc;
    public static String Napas_remoteFile;
    public static String Napas_localDir;
    public static String Napas_remoteDir;
    public static String Napas_publicKey;
    public static String Napas_file;
    public static String Napas_ContentFile;
    public static String Napas_SecretKey;
    
    public static String PaytechPrivateKey;
    public static String password;
    public static String alias;
    
    public static String ReportDir;
    public static String HeaderCSV;
    
    public static String Host;
    public static String EmailUsername;
    public static String EmailPassword;
    public static String EmailPort;
    public static String EmailAuth;
    public static String EmailStarttls;
    public static String EmailLists;
    
    
    @Value("${ReportEmailLists}")
    public void setEmailLists(String emailLists) {
		EmailLists = emailLists;
	}

	@Value("${spring.mail.host}")
	public void setHost(String host) {
		Host = host;
	}
    
    @Value("${spring.mail.username}")
	public void setEmailUsername(String emailUsername) {
		EmailUsername = emailUsername;
	}
    
    @Value("${spring.mail.password}")
	public void setEmailPassword(String emailPassword) {
		EmailPassword = emailPassword;
	}
    
    @Value("${spring.mail.port}")
	public void setEmailPort(String emailPort) {
		EmailPort = emailPort;
	}
    
    @Value("${spring.mail.properties.mail.smtp.auth}")
	public void setEmailAuth(String emailAuth) {
		EmailAuth = emailAuth;
	}
    
    @Value("${spring.mail.properties.mail.smtp.starttls.enable}")
	public void setEmailStarttls(String emailStarttls) {
		EmailStarttls = emailStarttls;
	}

	@Value("${Napas_remoteHost}")
    public void setNapas_remoteHost(String napas_remoteHost) {
    	this.Napas_remoteHost = napas_remoteHost;
	}
    
    @Value("${Napas_port}")
	public void setNapas_port(String napas_port) {
		this.Napas_port = napas_port;
	}
    
    @Value("${Napas_username}")
	public void setNapas_username(String napas_username) {
		this.Napas_username = napas_username;
	}

    @Value("${Napas_password}")
	public void setNapas_password(String napas_password) {
		this.Napas_password = napas_password;
	}

    @Value("${Napas_knownHostsFileLoc}")
	public void setNapas_knownHostsFileLoc(String napas_knownHostsFileLoc) {
		this.Napas_knownHostsFileLoc = napas_knownHostsFileLoc;
	}

    @Value("${Napas_remoteFile}")
	public void setNapas_remoteFile(String napas_remoteFile) {
		this.Napas_remoteFile = napas_remoteFile;
	}

    @Value("${Napas_localDir}")
	public void setNapas_localDir(String napas_localDir) {
		this.Napas_localDir = napas_localDir;
	}

    @Value("${Napas_remoteDir}")
	public void setNapas_remoteDir(String napas_remoteDir) {
		this.Napas_remoteDir = napas_remoteDir;
	}

    @Value("${Napas_publicKey}")
	public void setNapas_publicKey(String napas_publicKey) {
		this.Napas_publicKey = napas_publicKey;
	}

    @Value("${Napas_file}")
	public void setNapas_file(String napas_file) {
		this.Napas_file = napas_file;
	}

    @Value("${PaytechPrivateKey}")
	public void setPaytechPrivateKey(String paytechPrivateKey) {
		this.PaytechPrivateKey = paytechPrivateKey;
	}

    @Value("${password}")
	public void setPassword(String password) {
		this.password = password;
	}

    @Value("${alias}")
	public void setAlias(String alias) {
		this.alias = alias;
	}
    
    @Value("${Napas_ContentFile}")
    public void setNapas_ContentFile(String napas_ContentFile) {
		this.Napas_ContentFile = napas_ContentFile;
	}
    
    
    @Value("${Napas_SecretKey}")
	public void setNapas_SecretKey(String napas_SecretKey) {
		this.Napas_SecretKey = napas_SecretKey;
	}
    
    @Value("${Report_localDir}")
   	public void setReport_localDir(String reportDir) {
   		this.ReportDir = reportDir;
   	}
    
    @Value("${Report_headerCSV}")
   	public void setReport_headerCSV(String headerCSV) {
   		this.HeaderCSV = headerCSV;
   	}

	public static String pathFile(String date, String code, String service ,String type) {
        return String.format(Napas_file, date, code, service, type);
    }
}
